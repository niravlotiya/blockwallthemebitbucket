﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateShape : MonoBehaviour {

	// Use this for initialization

	public static RotateShape Instance;

	private void Awake() {
		Instance = this;
	}

	void Start () {
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnRotate(Transform shapeContainer){
		if(shapeContainer.GetChild(0)!=null){
		int rotatedShapeID = shapeContainer.GetChild(0).GetComponent<ShapeInfo>().rotatedShapeID;
		Destroy(shapeContainer.GetChild(0).gameObject);
		BlockShapeSpawner.Instance.CreateShapeWithID(shapeContainer,rotatedShapeID);
		}
	}

}
