﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "UI Theme", menuName = "BlockPuzzle/Create UI Theme", order = 1)]
public class UIImageTheme : ScriptableObject  
{
	public List<UIImageThemeTag> UIStyle;
}

[System.Serializable]
public class UIImageThemeTag
{
	public string tagName;
	public Sprite UIImage;
}