﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UI Theme", menuName = "BlockPuzzle/Create UI Theme", order = 1)]
public class UIThemeImages : ScriptableObject  
{
	public List<UIThemeImageTag> UIStyle;
}

[System.Serializable]
public class UIThemeImageTag
{
	public string tagName;
	//public Color UIColor;
	public Sprite UISprite;
}