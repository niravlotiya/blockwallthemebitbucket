﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_PURCHASING
using UnityEngine.Purchasing;
#endif

public class PurchaseRewardsManager : Singleton<PurchaseRewardsManager> 
{
	#if UNITY_PURCHASING
	public void ProcessRewardForProduct(Product product)
	{
		switch (product.definition.id) {
		case "100_coins_25_inr":
			CurrencyManager.Instance.AddCoinBalance (100);
			break;
		case "500_coins_0_99":
			CurrencyManager.Instance.AddCoinBalance (500);
			break;
		case "1000_coins_40_inr":
			CurrencyManager.Instance.AddCoinBalance (1000);
			break;		
		}
	}
	#endif
}
