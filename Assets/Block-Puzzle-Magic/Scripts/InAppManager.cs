﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


public class InAppManager : MonoBehaviour, IStoreListener
{

    private IStoreController m_StoreController;
    private IExtensionProvider m_StoreExtensionProvider;

    public List<string> packageDataList;

    // Use this for initialization
    void Start()
    {

        InitializeProducts();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitializeProducts()
    {
        if (IsInitialized())
            return;

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        for (int i = 0; i < packageDataList.Count; i++)
        {
            builder.AddProduct(packageDataList[i], ProductType.Consumable);
        }
        UnityPurchasing.Initialize(this, builder);
    }
    /// <summary>
    /// Called when Unity IAP is ready to make purchases.
    /// </summary>
    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("Initialized");
        // UIManager.instance.loadingPanel.SetActive (false);
        this.m_StoreController = controller;
        this.m_StoreExtensionProvider = extensions;
    }

    /// <summary>
    /// Called when Unity IAP encounters an unrecoverable initialization error.
    ///
    /// Note that this will not be called if Internet is unavailable; Unity IAP
    /// will attempt initialization until it becomes available.
    /// </summary>

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("Initialized Failed");
        // UIManager.instance.loadingPanel.SetActive (false);
    }

    /// <summary>
    /// Called when a purchase completes.
    ///
    /// May be called at any time after OnInitialized().
    /// </summary>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        Debug.Log("Process Purchase " + e.purchasedProduct.definition.id);
        if (e.purchasedProduct.definition.id.Equals("100_coins_25_inr"))
        {           
            OnPurchaseSuccessful(e.purchasedProduct);
        }
        else if (e.purchasedProduct.definition.id.Equals("500_coins_0_99"))
        {         
            OnPurchaseSuccessful(e.purchasedProduct);
        }
        else if (e.purchasedProduct.definition.id.Equals("1000_coins_40_inr"))
        {            
            OnPurchaseSuccessful(e.purchasedProduct);
        }

        if(string.Equals(e.purchasedProduct.definition.id,"monster_bird_5")){
            Debug.Log("Monster bird purchase successfull");
        }

        /* for(int i=0;i<PayToPlay.instance.packageDataList.Count;i++){
          if(e.purchasedProduct.definition.id.Equals(PayToPlay.instance.packageDataList[i].getName())){
              PayToPlay.instance.OnPurchaseItemSuccess( PayToPlay.instance.packageDataList[i].getId ()+"");
             }
         }
 */

        return PurchaseProcessingResult.Complete;
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    /// <summary>
    /// Called when a purchase fails.
    /// </summary>
    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        Debug.Log(" Purcahse Failuer" + p.ToString());
		StackManager.Instance.purchaseFailScreen.Activate();        
    }

    public void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    #if UNITY_PURCHASING
	public void OnPurchaseSuccessful(Product product)
	{
		StackManager.Instance.purchaseSuccessScreen.Activate();
		PurchaseRewardsManager.Instance.ProcessRewardForProduct (product);
	}
	#endif


     /* public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
	{
		StackManager.Instance.purchaseFailScreen.Activate();
    }*/
	
}
