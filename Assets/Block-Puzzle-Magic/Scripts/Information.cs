﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Information : MonoBehaviour
 {

	 public static Information instance;

	// Use this for initialization
	void Awake()
	{
		instance = this;
	}
	void Start ()
	 {
		
	}
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void OnInformationClose ()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();	
			InterstitialAdd.instance.RequestInterstitial();
			gameObject.Deactivate();
			
		}
	}

	 
}
