﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using System;

public class GameProgressManager : Singleton<GameProgressManager>
{
    /// <summary>
    /// Gets the previous session data.
    /// </summary>
    /// <returns>The previous session data.</returns>
    List<string> gameStatesList;
    List<List<int>> powersBackup;
    List<int> currentPowers;

    private XDocument presentState;
    public static GameProgressManager Instance;
    public static int undoCount = -1;

    public static int countOfMovesAfterNewShapesGenerated = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable() {
        ScrollViewList.OnShapeReplace += ShapeReplaced;
    }

    private void OnDisable() {
        ScrollViewList.OnShapeReplace -= ShapeReplaced;
    }
    

    public PreviousSessionData GetPreviousSessionData()
    {
        string oldProgrss = PlayerPrefs.GetString("GameProgress" + GameController.gameMode.ToString(), string.Empty);

        if (oldProgrss != string.Empty)
        {
            PreviousSessionData data = new PreviousSessionData();
            XDocument xDoc = XDocument.Parse(oldProgrss.ToString());

            if (xDoc != null)
            {
                #region game mode
                data.currentMode = (GameMode)Enum.Parse(typeof(GameMode), xDoc.Root.Element("ModeName").Value);
                #endregion

                #region block grid
                XElement blockDataElement = xDoc.Root.Element("BlockData");
                if (blockDataElement != null)
                {
                    var allRows = blockDataElement.Elements("Row");
                    foreach (XElement ele in allRows)
                    {
                        data.blockGridInfo.Add(ele.Value);
                    }
                }

                #endregion

                #region placing blocks
                data.shapeInfo = xDoc.Root.Element("ShapeInfo").Value;
                #endregion

                #region score
                data.score = xDoc.Root.Element("Score").Value.TryParseInt();

                #endregion

                #region total rescue done
                data.totalRescueDone = xDoc.Root.Element("TotalRescueDone").Value.TryParseInt();
                #endregion

                #region total free rescue done
                data.totalFreeRescueDone = xDoc.Root.Element("TotalFreeRescueDone").Value.TryParseInt();
                #endregion

                #region placed bombs
                if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE)
                {
                    XElement placedBombElement = xDoc.Root.Element("PlacedBombs");
                    if (placedBombElement != null)
                    {
                        var allPlacedBombs = placedBombElement.Elements("Bomb");

                        foreach (XElement ele in allPlacedBombs)
                        {
                            data.placedBombInfo.Add(ConverToPlacedBombInfo(ele));
                        }
                    }
                }
                #endregion

                #region remaining time
                if (GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
                {
                    data.remainingTime = xDoc.Root.Element("RemainingTime").Value.TryParseInt(60);
                }
                #endregion

                #region total rescue done
                data.movesCount = xDoc.Root.Element("MovesCount").Value.TryParseInt();
                #endregion

                return data;
            }
        }
        return null;
    }

    /// <summary>
    /// Convers to placed bomb info.
    /// </summary>
    /// <returns>The to placed bomb info.</returns>
    /// <param name="ele">Ele.</param>
    public PlacedBomb ConverToPlacedBombInfo(XElement ele)
    {
        return new PlacedBomb(ele.Attribute("rowID").Value.TryParseInt(), ele.Attribute("columnID").Value.TryParseInt(), ele.Attribute("bombCounter").Value.TryParseInt());
    }

    /// <summary>
    /// Raises the application pause event.
    /// </summary>
    /// <param name="pause">If set to <c>true</c> pause.</param>
    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            SaveGame();
        }
    }

    /// <summary>
    /// Saves the game.
    /// </summary>
    public void SaveGame()
    {
        XDocument xDocProgress = XDocument.Parse(Resources.Load("BoardTemplate").ToString());
        if (xDocProgress != null)
        {

            #region game mode
            XElement GameModeElement = xDocProgress.Root.Element("ModeName");
            GameModeElement.Value = GameController.gameMode.ToString();
            #endregion

            #region block grid
            XElement blockDataElement = xDocProgress.Root.Element("BlockData");
            int index = 0;

            for (int rowID = 0; rowID < GameBoardGenerator.Instance.TotalRows; rowID++)
            {
                string row = "";
                for (int columnID = 0; columnID < GameBoardGenerator.Instance.TotalColumns; columnID++)
                {
                    row += (GamePlay.Instance.blockGrid[index].blockID + ",");
                    index++;
                }
                row = row.Remove(row.Length - 1);
                blockDataElement.Add(CreateXElement("Row", row));
            }
            #endregion

            #region placing blocks
            xDocProgress.Root.Element("ShapeInfo").Value = BlockShapeSpawner.Instance.GetAllOnBoardShapeNames();
            #endregion


            #region score
            xDocProgress.Root.Element("Score").Value = ScoreManager.Instance.GetScore().ToString();
            #endregion

            #region total rescue done
            xDocProgress.Root.Element("TotalRescueDone").Value = GamePlay.Instance.TotalRescueDone.ToString();
            #endregion

            #region total free rescue done
            xDocProgress.Root.Element("TotalFreeRescueDone").Value = GamePlay.Instance.TotalFreeRescueDone.ToString();
            #endregion

            #region placed bombs
            if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE)
            {
                XElement placedBombsElement = xDocProgress.Root.Element("PlacedBombs");
                List<Block> bombBlocks = GamePlay.Instance.blockGrid.FindAll(o => o.isBomb);
                foreach (Block block in bombBlocks)
                {
                    placedBombsElement.Add(CreateBombElement(block.rowID, block.columnID, block.bombCounter));
                }
            }
            #endregion

            #region remaining time
            if (GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
            {
                xDocProgress.Root.Element("RemainingTime").Value = GamePlay.Instance.timeSlider.GetRemainingTime().ToString();
            }
            #endregion

            #region moves count
            xDocProgress.Root.Element("MovesCount").Value = GamePlay.Instance.MoveCount.ToString();
            #endregion

            #region save progress
            PlayerPrefs.SetString("GameProgress" + GameController.gameMode.ToString(), xDocProgress.ToString());
            #endregion
        }
    }

    /// <summary>
    /// Clears the progress.
    /// </summary>
    public void ClearProgress()
    {
        PlayerPrefs.DeleteKey("GameProgress" + GameController.gameMode.ToString());
    }

    /// <summary>
    /// Creates the X element.
    /// </summary>
    /// <returns>The X element.</returns>
    /// <param name="elementName">Element name.</param>
    /// <param name="value">Value.</param>
    XElement CreateXElement(string elementName, string value)
    {
        XElement ele = new XElement(elementName);
        ele.Value = value;
        return ele;
    }

    /// <summary>
    /// Creates the bomb element.
    /// </summary>
    /// <returns>The bomb element.</returns>
    /// <param name="rowID">Row I.</param>
    /// <param name="columnID">Column I.</param>
    /// <param name="bombCounter">Bomb counter.</param>
    XElement CreateBombElement(int rowID, int columnID, int bombCounter)
    {
        XElement ele = new XElement("Bomb");
        ele.Add(new XAttribute("rowID", rowID));
        ele.Add(new XAttribute("columnID", columnID));
        ele.Add(new XAttribute("bombCounter", bombCounter));
        return ele;
    }

    private void Start()
    {
        gameStatesList = new List<string>();
        powersBackup = new List<List<int>>();
    }

    public void OnExecuteUndo()
    {
        Debug.Log(" On Undo Call");
        PreviousSessionData previousSession = PerformUndo();
        //Debug.Log(GameBoardGenerator.Instance.previousSessionData.ToString());
        if (previousSession != null)
        {
            Debug.Log(" previous Session != Null Called");
            //ScoreManager.Instance.AddScore(-GameBoardGenerator.Instance.previousSessionData.score*GamePlay.Instance.Sc,false);
            GameBoardGenerator.Instance.UndoBoardStatus(previousSession);
            BlockShapeSpawner.Instance.ClearShapeContainers();
            BlockShapeSpawner.Instance.UndoShapes(previousSession);
        }

    }
    public void SaveGameState()
    {
        XDocument xDocProgress = XDocument.Parse(Resources.Load("BoardTemplate").ToString());
        if (xDocProgress != null)
        {

            #region game mode
            XElement GameModeElement = xDocProgress.Root.Element("ModeName");
            GameModeElement.Value = GameController.gameMode.ToString();
            #endregion

            #region block grid
            XElement blockDataElement = xDocProgress.Root.Element("BlockData");
            int index = 0;

            for (int rowID = 0; rowID < GameBoardGenerator.Instance.TotalRows; rowID++)
            {
                string row = "";
                for (int columnID = 0; columnID < GameBoardGenerator.Instance.TotalColumns; columnID++)
                {
                    row += (GamePlay.Instance.blockGrid[index].blockID + ",");
                    index++;
                }
                row = row.Remove(row.Length - 1);
                blockDataElement.Add(CreateXElement("Row", row));
            }
            #endregion

            #region placing blocks
            xDocProgress.Root.Element("ShapeInfo").Value = BlockShapeSpawner.Instance.GetAllOnBoardShapeNames();
            #endregion


            #region score
            xDocProgress.Root.Element("Score").Value = ScoreManager.Instance.GetScore().ToString();
            #endregion

            #region total rescue done
            xDocProgress.Root.Element("TotalRescueDone").Value = GamePlay.Instance.TotalRescueDone.ToString();
            #endregion

            #region total free rescue done
            xDocProgress.Root.Element("TotalFreeRescueDone").Value = GamePlay.Instance.TotalFreeRescueDone.ToString();
            #endregion

            #region placed bombs
            if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE)
            {
                XElement placedBombsElement = xDocProgress.Root.Element("PlacedBombs");
                List<Block> bombBlocks = GamePlay.Instance.blockGrid.FindAll(o => o.isBomb);
                foreach (Block block in bombBlocks)
                {
                    placedBombsElement.Add(CreateBombElement(block.rowID, block.columnID, block.bombCounter));
                }
            }
            #endregion

            #region remaining time
            if (GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
            {
                xDocProgress.Root.Element("RemainingTime").Value = GamePlay.Instance.timeSlider.GetRemainingTime().ToString();
            }
            #endregion

            #region moves count
            xDocProgress.Root.Element("MovesCount").Value = GamePlay.Instance.MoveCount.ToString();
            #endregion

            #region save progress
            gameStatesList.Add(xDocProgress.ToString());
            presentState = xDocProgress;
            //currentPowers = new List<int>();
            // currentPowers.Add(PowerManager.Instance.GetMagicCubeBalance());
            // currentPowers.Add(PowerManager.Instance.GetRainbowBallBalance());
            // powersBackup.Add(currentPowers);
            undoCount++;

            //Debug.Log("SAVE undocount increased= " + undoCount + " statelist add= " + gameStatesList.Count);
            //gameStatesList.count();
            //Debug.Log("Success");
            //Debug.Log(gameStatesList.count);
            //Debug.Log(gameStatesList[undoCount]);
            //Debug.Log(undoCount);
            #endregion
        }
    }

    public void ResetUndoData()
    {
        gameStatesList.Clear();
        undoCount = -1;
    }

    public PreviousSessionData PerformUndo()
    {
        if (undoCount > 0)
        {
            string oldProgrss = gameStatesList[undoCount - 1];
            //currentPowers = powersBackup[undoCount-1];
            if (oldProgrss != string.Empty)
            {
                Debug.Log("Perform undo");
                PreviousSessionData data = new PreviousSessionData();
                XDocument xDoc = XDocument.Parse(oldProgrss.ToString());
                // PowerManager.Instance.SetMagicCubeBalance(currentPowers[0]);
                // PowerManager.Instance.SetRainbowBallBalance(currentPowers[1]);

                foreach (Block b in GamePlay.Instance.blockGrid)
                {
                    b.ClearBlock();
                    Destroy(b.gameObject);
                }
                GamePlay.Instance.blockGrid.Clear();

                if (xDoc != null)
                {
                    #region game mode
                    data.currentMode = (GameMode)Enum.Parse(typeof(GameMode), xDoc.Root.Element("ModeName").Value);
                    #endregion

                    #region block grid
                    XElement blockDataElement = xDoc.Root.Element("BlockData");
                    if (blockDataElement != null)
                    {
                        var allRows = blockDataElement.Elements("Row");
                        foreach (XElement ele in allRows)
                        {
                            data.blockGridInfo.Add(ele.Value);
                        }
                    }

                    #endregion

                    #region placing blocks
                    data.shapeInfo = xDoc.Root.Element("ShapeInfo").Value;
                    #endregion

                    #region score
                    data.score = xDoc.Root.Element("Score").Value.TryParseInt();

                    #endregion

                    #region total rescue done
                    data.totalRescueDone = xDoc.Root.Element("TotalRescueDone").Value.TryParseInt();
                    #endregion

                    #region total free rescue done
                    data.totalFreeRescueDone = xDoc.Root.Element("TotalFreeRescueDone").Value.TryParseInt();
                    #endregion

                    #region placed bombs
                    if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE)
                    {
                        XElement placedBombElement = xDoc.Root.Element("PlacedBombs");
                        if (placedBombElement != null)
                        {
                            var allPlacedBombs = placedBombElement.Elements("Bomb");

                            foreach (XElement ele in allPlacedBombs)
                            {
                                data.placedBombInfo.Add(ConverToPlacedBombInfo(ele));
                            }
                        }
                    }
                    #endregion

                    #region remaining time
                    if (GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
                    {
                        data.remainingTime = xDoc.Root.Element("RemainingTime").Value.TryParseInt(60);
                    }
                    #endregion

                    #region total rescue done
                    data.movesCount = xDoc.Root.Element("MovesCount").Value.TryParseInt();
                    #endregion

                    gameStatesList.RemoveAt(gameStatesList.Count - 1);
                    undoCount--;

                    Debug.Log("REMOVE undocount decreased= " + undoCount + " statelist remove= " + gameStatesList.Count);

                    return data;
                }
            }
            return null;
        }
        return null;
    }

    public IEnumerator UpdatePresentState()
    {
        yield return new WaitForSeconds(0.5F);
        if (gameStatesList.Count > 0)
        {
            presentState = XDocument.Parse(gameStatesList[gameStatesList.Count - 1]);
            presentState.Root.Element("ShapeInfo").Value = BlockShapeSpawner.Instance.GetAllOnBoardShapeNames();
            gameStatesList[gameStatesList.Count - 1] = presentState.ToString();
        }

    }

    public IEnumerator UpdateStatesOnReplace()
    {
        yield return new WaitForSeconds(0.5F);
        XDocument tempState;
        string tempShapesList;
        int changeStateIndex;
        Debug.Log("countofmoves  " + countOfMovesAfterNewShapesGenerated);

        for(int index = 0 ; index < countOfMovesAfterNewShapesGenerated + 1 ; index++)
        {
            changeStateIndex = gameStatesList.Count - index - 1;
            tempState = XDocument.Parse(gameStatesList[changeStateIndex]);
            tempShapesList = tempState.Root.Element("ShapeInfo").Value;
            Debug.Log("before " + tempShapesList);
            Debug.Log(ShapeReplaceManager.currentShapeId);
            Debug.Log(ShapeReplaceManager.replacementShapeId);
            tempShapesList = tempShapesList.Replace(ShapeReplaceManager.currentShapeId.ToString(),ShapeReplaceManager.replacementShapeId.ToString());
            Debug.Log("after " + tempShapesList);
            tempState.Root.Element("ShapeInfo").Value = tempShapesList;
            gameStatesList[changeStateIndex] = tempState.ToString();
        }
    }

    public void ShapeReplaced()
    {
        StartCoroutine(UpdateStatesOnReplace());
    }
}


/// <summary>
/// Previous session data.
/// </summary>
public class PreviousSessionData
{
    public GameMode currentMode;
    public List<string> blockGridInfo = new List<string>();
    public string shapeInfo = string.Empty;
    public int score = 0;
    public int totalRescueDone = 0;
    public int totalFreeRescueDone = 0;
    public int coinsEarned = 0;
    public List<PlacedBomb> placedBombInfo = new List<PlacedBomb>();
    public int remainingTime = 0;
    public int movesCount = 0;
}

/// <summary>
/// Placed bomb.
/// </summary>
public class PlacedBomb
{
    public int rowID;
    public int columnID;
    public int bombCounter;

    public PlacedBomb(int _rowID, int _columnID, int _bombCounter)
    {
        rowID = _rowID;
        columnID = _columnID;
        bombCounter = _bombCounter;
    }
}
