﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager> 
{
	[SerializeField] public Text txtScore;
	[SerializeField] public Text txtCoins;
	[SerializeField] private GameObject scoreAnimator;

	[SerializeField] private GameObject coinAnimator;
	[SerializeField] private Text txtAnimatedText;

	[SerializeField] private Text txtAnimatedTextCoins;
	
	[SerializeField] private Text txtBestScore;
	
	[HideInInspector] public int Score = 0;

	private const int TRUE = 1;
	private const int FALSE= 0;
	int bestScore = 0;

	void Start()
	{
		txtScore.text = Score.ToString ();	
		txtCoins.text=CurrencyManager.Instance.GetCoinBalance ().ToString();
		int bestScore = PlayerPrefs.GetInt ("BestScore_" + GameController.gameMode.ToString (), Score);
		txtBestScore.text = bestScore.ToString();
	}

	public void AddScore(int scoreToAdd, bool doAnimate = true)
	{
		int oldScore = Score;
		Score += scoreToAdd;

		int oldCoins=CurrencyManager.Instance.GetCoinBalance();
		if(Score>02000 && !GamePlay.Instance.isScore2000Up && PlayerPrefs.GetInt("isScore2000Up",FALSE)==0){
			GamePlay.Instance.isScore2000Up=true;
			PlayerPrefs.SetInt("isScore2000Up",TRUE);
			CurrencyManager.Instance.AddCoinBalance(25);
			StartCoroutine (SetCoins(oldCoins, CurrencyManager.Instance.GetCoinBalance(),25));	
		}else if(Score>=5000 && !GamePlay.Instance.isScore5000Up && PlayerPrefs.GetInt("isScore5000Up",FALSE)==0){
			PlayerPrefs.SetInt("isScore5000Up",TRUE);
			GamePlay.Instance.isScore5000Up=true;
			CurrencyManager.Instance.AddCoinBalance(50);
			StartCoroutine (SetCoins(oldCoins, CurrencyManager.Instance.GetCoinBalance(),50));			
		}

		StartCoroutine (SetScore(oldScore, Score));
		

		if (doAnimate) {
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0;
			scoreAnimator.transform.position = mousePos;			
			txtAnimatedText.text = "+" + scoreToAdd.ToString ();
			scoreAnimator.SetActive (true);
		}
	}

	private void setAnimateCoinTextPosition(int coinsToAdd){
			Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			mousePos.z = 0;
			coinAnimator.transform.position = mousePos;			
			txtAnimatedTextCoins.text = "+" + coinsToAdd.ToString ();
			coinAnimator.SetActive (true);
	}
	public int GetScore()
	{
		return Score;
	}

	IEnumerator SetCoins(int lastCoins, int currentCoins,int coinsToAdd){
		yield return new WaitForSeconds(1);
		setAnimateCoinTextPosition(coinsToAdd);
		int IterationSize = (currentCoins - lastCoins) / 10;

		for (int index = 1; index < 10; index++) {
			lastCoins += IterationSize;
			txtCoins.text =  string.Format("{0:#,0}", lastCoins);
			yield return new WaitForEndOfFrame ();
		}
		txtCoins.text =  string.Format("{0:#,0}", currentCoins);
		yield return new WaitForSeconds (0.5F);
		coinAnimator.SetActive (false);
	}
	IEnumerator SetScore(int lastScore, int currentScore)
	{
		int IterationSize = (currentScore - lastScore) / 10;

		for (int index = 1; index < 10; index++) {
			lastScore += IterationSize;
			txtScore.text =  string.Format("{0:#,0}", lastScore);
			yield return new WaitForEndOfFrame ();
		}
		txtScore.text =  string.Format("{0:#,0}", currentScore);
		yield return new WaitForSeconds (1F);
		scoreAnimator.SetActive (false);
	}

	public void SetScoreNoAnimation(int undoScore){
		Score = undoScore;
		txtScore.text = string.Format("{0:#,0}", undoScore);
	}

	public void ResetPrefsOnGameOver()
	{
		PlayerPrefs.SetInt("isScore2000Up",FALSE);
		PlayerPrefs.SetInt("isScore5000Up",FALSE);
	}
}