﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckForUpdates : MonoBehaviour
{

    public static CheckForUpdates Instance;

    public Text titleText;

    public Text yesButtonText;

    public Text noButtonText;

    public Text messageText;

    public GameObject playStoreButton;

    public GameObject cancelButton;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCancelButton()
    {
        gameObject.SetActive(false);
    }

    public void OnPlayStoreButton()
    {
        if (GameController.Instance.isInternetAvailable())
        {
            Application.OpenURL(GameInfo.ReviewURL);
        }
    }
}
