﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Xml.Linq;

#if HBDOTween
using DG.Tweening;
#endif

// This script has main logic to run entire gameplay.
public class GamePlay1 : Singleton<GamePlay>,IPointerDownHandler,IPointerUpHandler, IBeginDragHandler,IDragHandler
{
	ShapeInfo powerShape = null;
	[HideInInspector]
	public List<Block> blockGrid;

	ShapeInfo currentShape = null;
	Transform hittingBlock = null;

	List<Block> highlightingBlocks;
	List<Block> powerPlaceBlocks;

	public AudioClip blockPlaceSound;
	public AudioClip blockSelectSound;

	public int scoreMultiplyier=1;
	// Line break sounds.
	[SerializeField] private AudioClip lineClear1;
	[SerializeField] private AudioClip lineClear2;
	[SerializeField] private AudioClip lineClear3;
	[SerializeField] private AudioClip lineClear4;

	public AudioClip blockNotPlacedSound;

	[Tooltip("Max no. of times rescue can be used in 1 game. -1 is infinite")]
	[SerializeField] public int MaxAllowedRescuePerGame = 0;

	[Tooltip("Max no. of times rescue can be used in 1 game using watch video. -1 is infinite")]
	[SerializeField] public int MaxAllowedVideoWatchRescue = 0;

	[SerializeField] public int MaxAllowedPaidRescue=0;



	[HideInInspector]
	public int TotalFreeRescueDone = 0;
	
	[HideInInspector]
	public int TotalRescueDone = 0;

	[HideInInspector]
	public int MoveCount = 0;

	public Sprite BombSprite;
	public BlockPuzzleTimer timeSlider;

	public  bool isHelpOnScreen = false;

	public bool isBreaking=false;

	public bool isScore2000Up=false;

	public bool isScore5000Up=false;

	public Button MagicCubeButton;
	public Button MagicCubeCancelButton;
	public ShapeInfo MagicCube;			

	public Image BlockShapePanel;
    public Button RainbowBallButton;

	public Text boardName;

	private bool isClick=false;
	public bool allowRotation = true;

	void Start ()
	{
		string eventName = "GamePlay";

		string savedBoardName=PlayerPrefs.GetString(GameController.BOARD_NAME);
		if(savedBoardName.Equals("")){
			boardName.text="Block Wall";
		}else{
			boardName.text=savedBoardName+"'s Board";
		}

		//Generate board from GameBoardGenerator Script Component.
		GetComponent<GameBoardGenerator> ().GenerateBoard ();
		highlightingBlocks = new List<Block> ();
		powerPlaceBlocks = new List<Block>();
		Debug.Log ("Banner Being Add Called");		
		BannerAdd.instance.RequestBanner ();
		Debug.Log ("Banner Add Called");

		#region time mode
		// Timer will start with TIME and CHALLENGE mode.
		if(GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
		{
			timeSlider.gameObject.SetActive(true);
		}
		#endregion
		#region check for help
		Invoke("CheckForHelp",0.5F);
		#endregion
	}

	#region IBeginDragHandler implementation

	/// <summary>
	/// Raises the begin drag event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnBeginDrag (PointerEventData eventData)
	{
		
		if (currentShape != null) {
			Vector3 pos = Camera.main.ScreenToWorldPoint (eventData.position);
			pos.z = currentShape.transform.localPosition.z;
			currentShape.transform.localPosition = pos;
		}
	}

	#endregion

	#region IPointerDownHandler implementation
	/// <summary>
	/// Raises the pointer down event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerDown (PointerEventData eventData)
	{
		if(isBreaking)
			return;

		if(allowRotation){
			if(!isClick){
				isClick = true;
			}
		}

		else{
			ElevateShape(eventData);
		}

		
	}
	#endregion

	void ElevateShape(PointerEventData clickData){
		if (clickData.pointerCurrentRaycast.gameObject != null) {
			Transform clickedObject = clickData.pointerCurrentRaycast.gameObject.transform;

			if (clickedObject.GetComponent<ShapeInfo>() != null) 
			{
				if (clickedObject.transform.childCount > 0) {
					// Debug.Log("OnPointerDown ");
					currentShape = clickedObject.GetComponent<ShapeInfo>();
					Vector3 pos = Camera.main.ScreenToWorldPoint (clickData      .position);
					currentShape.transform.localScale = Vector3.one;
					currentShape.transform.position = new Vector3 (pos.x,  (pos.y + 1.5F), 0);
					AudioManager.Instance.PlaySound (blockSelectSound);

					if (isHelpOnScreen) {
						GetComponent<InGameHelp> ().StopHelp ();
					}
				}
			}
		}
	}

	#region IPointerUpHandler implementation
	/// <summary>
	/// Raises the pointer up event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnPointerUp (PointerEventData eventData)
	{
		Debug.Log("PointerUP");

		if(isBreaking)
			return;

		if(allowRotation&&isClick){
			ClickResolver(eventData);
		}
		
		if (currentShape != null) {

			if (highlightingBlocks.Count > 0 || powerPlaceBlocks.Count > 0) 
			{
				if(powerShape != null){  
					Destroy (powerShape.gameObject);
					powerShape = null;
					ExecuteRescue(0);
					UIUpdateOnMagicCube();
					powerPlaceBlocks.Clear();
					PowerManager.Instance.UseMagicCube();

				}      
				SetImageToPlacingBlocks ();
				Destroy (currentShape.gameObject);
				currentShape = null;

				if(highlightingBlocks.Count > 0)   
					MoveCount += 1;

				Invoke ("CheckBoardStatus", 0.1F);
			} else {
				#if HBDOTween
				currentShape.transform.DOLocalMove (Vector3.zero, 0.5F);
				currentShape.transform.DOScale (Vector3.one * 0.5F, 0.5F);
				#endif
				currentShape = null;
				AudioManager.Instance.PlaySound (blockNotPlacedSound);
			}
		}
	}
	#endregion

	#region IDragHandler implementation
	/// <summary>
	/// Raises the drag event.
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnDrag (PointerEventData eventData)
	{
		if(allowRotation&&isClick){
			isClick = false;
			ElevateShape(eventData);
			return;
		}

		if (currentShape != null) {

			Vector3 pos = Camera.main.ScreenToWorldPoint (eventData.position);
			pos = new Vector3(pos.x, (pos.y + 1.5F),0F);

			currentShape.transform.position = pos;

			RaycastHit2D hit = Physics2D.Raycast (currentShape.GetComponent<ShapeInfo>().firstBlock.block.position, Vector2.zero, 1);

			if (hit.collider != null) 
			{
				if (hittingBlock == null || hit.collider.transform != hittingBlock) {
					hittingBlock = hit.collider.transform;
					CanPlaceShape (hit.collider.transform);
				}
				if(powerShape != null)
				{
					CanPlaceShape(hit.collider.transform);
				}				
					
			} else 
			{
				StopHighlighting ();
				if(powerShape!=null)
					StopPowerHighlighting(); 
			}
		}
	}
	#endregion

	public void ClickResolver(PointerEventData clickData){
		
		Debug.Log("Clickresolver called");
		if(isClick)
		{
			Debug.Log("isClick True");
			isClick = false;
			if(clickData.pointerCurrentRaycast.gameObject!=null){
			Debug.Log("gameobject is not null");

			if(clickData.pointerCurrentRaycast.gameObject.transform.parent.gameObject!=null){
				Debug.Log("parent is not null");
				Debug.Log(clickData.pointerCurrentRaycast.gameObject.transform.parent.gameObject.name);
			}

			if(clickData.pointerCurrentRaycast.gameObject.transform.parent.gameObject.CompareTag("ShapeContainer")){
				Debug.Log("ShapeContainer");
				RotateShape.Instance.OnRotate(clickData.pointerCurrentRaycast.gameObject.transform.parent);
				Debug.Log("Rotate");}
			}
		}

	}

	/// <summary>
	/// Determines whether this instance can place shape the specified currentHittingBlock.
	/// </summary>
	public bool CanPlaceShape (Transform currentHittingBlock)
	{
		Block currentCell = currentHittingBlock.GetComponent<Block> ();

		int currentRowID = currentCell.rowID;
		int currentColumnID = currentCell.columnID;

		StopHighlighting ();

		if(powerShape!=null)
		{
			StopPowerHighlighting();	
		}
			

		bool canPlaceShape = true;
		foreach (ShapeBlock c in currentShape.ShapeBlocks) 
		{
			Block checkingCell = blockGrid.Find (o => o.rowID == currentRowID + (c.rowID + currentShape.startOffsetX) && o.columnID == currentColumnID + (c.columnID - currentShape.startOffsetY));
			
			if(powerShape != null && checkingCell != null)
			{     
			 	canPlaceShape=true;
				if(!powerPlaceBlocks.Contains(checkingCell))
				{
					powerPlaceBlocks.Add(checkingCell);
				}
			 	continue;
			 }	

			if ((checkingCell == null) || (checkingCell != null && checkingCell.isFilled)) {
				canPlaceShape = false;
				highlightingBlocks.Clear ();				
				break;
			} else {
				if (!highlightingBlocks.Contains (checkingCell)) {
					highlightingBlocks.Add (checkingCell);
				}
			}
		}

		if (canPlaceShape) 
		{
			SetHighLightImage ();
			if(powerShape != null)
				SetPowerHighLightImage();
		}

		return canPlaceShape;
	}

	/// <summary>
	/// Sets the high light image.
	/// </summary>
	void SetHighLightImage ()
	{
		foreach (Block c in highlightingBlocks) {
			c.SetHighlightImage (currentShape.blockImage);
		}
	}

	void SetPowerHighLightImage(){    
		foreach (Block c in powerPlaceBlocks) 
		{
			c.SetPowerHighlighting (powerShape.blockImage);
		}
	}

	void StopPowerHighlighting ()  
	{
		if (powerPlaceBlocks != null && powerPlaceBlocks.Count > 0)
		 {
			foreach (Block c in powerPlaceBlocks) 
			{
				c.StopPowerHighlighting ();
			}
		}
		//hittingBlock = null;
		powerPlaceBlocks.Clear ();
	}

	/// <summary>
	/// Stops the highlighting.
	/// </summary>
	void StopHighlighting ()
	{
		if (highlightingBlocks != null && highlightingBlocks.Count > 0) {
			foreach (Block c in highlightingBlocks) {
				c.StopHighlighting ();
			}
		}
		hittingBlock = null;
		highlightingBlocks.Clear ();
	}

	/// <summary>
	/// Sets the image to placing blocks.
	/// </summary>
	void SetImageToPlacingBlocks ()
	{
		if (highlightingBlocks != null && highlightingBlocks.Count > 0) 
		{
			foreach (Block c in highlightingBlocks) {
				c.SetBlockImage (currentShape.blockImage,currentShape.ShapeID);
			}
		}

		if(powerPlaceBlocks != null && powerPlaceBlocks.Count > 0)     
		{
			foreach (Block c in powerPlaceBlocks) {
				c.SetBlockImage (currentShape.blockImage,currentShape.ShapeID);
			}
		}     		
		AudioManager.Instance.PlaySound (blockPlaceSound);
	}

	/// <summary>
	/// Checks the board status.
	/// </summary>
	void CheckBoardStatus()
	{
		int placingShapeBlockCount = highlightingBlocks.Count;
		List<int> updatedRows = new List<int> ();
		List<int> updatedColumns = new List<int> ();

		List<List<Block>> breakingRows = new List<List<Block>> ();
		List<List<Block>> breakingColumns = new List<List<Block>> ();

		foreach (Block b in highlightingBlocks) {
			if (!updatedRows.Contains (b.rowID)) {
				updatedRows.Add(b.rowID);
			}
			if (!updatedColumns.Contains (b.columnID)) {
				updatedColumns.Add(b.columnID);
			}
		}
		highlightingBlocks.Clear ();
		powerPlaceBlocks.Clear(); 

		foreach(int rowID in updatedRows) {
			List<Block> currentRow = GetEntireRow (rowID);
			if (currentRow != null) {
				breakingRows.Add (currentRow);
			}
		}

		foreach (int columnID in updatedColumns) {
			List<Block> currentColumn = GetEntireColumn (columnID);
			if (currentColumn != null) {
				breakingColumns.Add (currentColumn);
			}
		}

		if (breakingRows.Count > 0 || breakingColumns.Count > 0) {
			StartCoroutine(BreakAllCompletedLines(breakingRows, breakingColumns,placingShapeBlockCount));
		} else {
			BlockShapeSpawner.Instance.FillShapeContainer ();
			ScoreManager.Instance.AddScore (scoreMultiplyier * placingShapeBlockCount);
		}

		if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE) {
			Invoke ("UpdateBlockCount", 0.5F);
		}
	}

	/// <summary>
	/// Gets the entire row.
	/// </summary>
	/// <returns>The entire row.</returns>
	/// <param name="rowID">Row I.</param>
	List<Block> GetEntireRow(int rowID)
	{
		List<Block> thisRow = new List<Block> ();
		for (int columnIndex = 0; columnIndex < GameBoardGenerator.Instance.TotalColumns; columnIndex++) {
			Block block = blockGrid.Find (o => o.rowID == rowID && o.columnID == columnIndex); 

			if (block.isFilled) {
				thisRow.Add (block);
			} else {
				return null;
			}
		}
		return thisRow;	
	}

	/// <summary>
	/// Gets the entire row for rescue.
	/// </summary>
	/// <returns>The entire row for rescue.</returns>
	/// <param name="rowID">Row I.</param>
	List<Block> GetEntireRowForRescue(int rowID)
	{
		List<Block> thisRow = new List<Block> ();
		for (int columnIndex = 0; columnIndex < GameBoardGenerator.Instance.TotalColumns; columnIndex++) {
			Block block = blockGrid.Find (o => o.rowID == rowID && o.columnID == columnIndex); 
			thisRow.Add (block);
		}
		return thisRow;	
	}

	/// <summary>
	/// Gets the entire column.
	/// </summary>
	/// <returns>The entire column.</returns>
	/// <param name="columnID">Column I.</param>
	List<Block> GetEntireColumn(int columnID)
	{
		List<Block> thisColumn = new List<Block> ();
		for (int rowIndex = 0; rowIndex < GameBoardGenerator.Instance.TotalRows; rowIndex++) {
			Block block = blockGrid.Find (o => o.rowID == rowIndex && o.columnID == columnID); 
			if (block.isFilled) {
				thisColumn.Add (block);
			} else {
				return null;
			}
		}
		return thisColumn;	
	}

	/// <summary>
	/// Gets the entire column for rescue.
	/// </summary>
	/// <returns>The entire column for rescue.</returns>
	/// <param name="columnID">Column I.</param>
	List<Block> GetEntireColumnForRescue(int columnID)
	{
		List<Block> thisColumn = new List<Block> ();
		for (int rowIndex = 0; rowIndex < GameBoardGenerator.Instance.TotalRows; rowIndex++) {
			Block block = blockGrid.Find (o => o.rowID == rowIndex && o.columnID == columnID); 
			thisColumn.Add (block);
		}
		return thisColumn;	
	}

	/// <summary>
	/// Breaks all completed lines.
	/// </summary>
	/// <returns>The all completed lines.</returns>
	/// <param name="breakingRows">Breaking rows.</param>
	/// <param name="breakingColumns">Breaking columns.</param>
	/// <param name="placingShapeBlockCount">Placing shape block count.</param>
	IEnumerator BreakAllCompletedLines(List<List<Block>> breakingRows, List<List<Block>> breakingColumns, int placingShapeBlockCount)
	{
		Debug.Log("Breaking Start ");
		isBreaking=true;
		int TotalBreakingLines = breakingRows.Count + breakingColumns.Count;
	
		if (TotalBreakingLines == 1) {
			AudioManager.Instance.PlaySound (lineClear1);
			ScoreManager.Instance.AddScore (10 + (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines == 2) {
			AudioManager.Instance.PlaySound (lineClear2);
			ScoreManager.Instance.AddScore (30+ (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines == 3) {
			AudioManager.Instance.PlaySound (lineClear3);
			ScoreManager.Instance.AddScore (60+ (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines >= 4) {
			AudioManager.Instance.PlaySound (lineClear4);
			if (TotalBreakingLines == 4) {
				ScoreManager.Instance.AddScore (100+ (placingShapeBlockCount * scoreMultiplyier));
			} else {
				ScoreManager.Instance.AddScore (300 + (placingShapeBlockCount * scoreMultiplyier));
			}
		}

		yield return 0;
		if (breakingRows.Count > 0) {
			foreach(List<Block> thisLine in breakingRows) {
				StartCoroutine(BreakThisLine (thisLine));
				yield return 0;				
			}	
		}
		yield return 0;		
		if (breakingRows.Count > 0) {
			yield return new WaitForSeconds (0.01F);
		}

		if (breakingColumns.Count > 0) {
			foreach(List<Block> thisLine in breakingColumns){
				StartCoroutine(BreakThisLine (thisLine));
				yield return 0;				
			}	
		}
		yield return 0;

		BlockShapeSpawner.Instance.FillShapeContainer ();
		yield return 0;

		Debug.Log("Breaking Ends ");
		isBreaking=false;
		#region time mode
		if(GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE){
			timeSlider.AddSeconds(TotalBreakingLines * 5);
		}		
		#endregion
	}

IEnumerator BreakAllCompletedLinesRescue(List<List<Block>> breakingRows, List<List<Block>> breakingColumns, int placingShapeBlockCount)
	{
		Debug.Log("Breaking Start ");
		isBreaking=true;
		int TotalBreakingLines = breakingRows.Count + breakingColumns.Count;
	
		if (TotalBreakingLines == 1) {
			AudioManager.Instance.PlaySound (lineClear1);
			// ScoreManager.Instance.AddScore (10 + (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines == 2) {
			AudioManager.Instance.PlaySound (lineClear2);
			// ScoreManager.Instance.AddScore (30+ (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines == 3) {
			AudioManager.Instance.PlaySound (lineClear3);
			// ScoreManager.Instance.AddScore (60+ (placingShapeBlockCount * scoreMultiplyier));
		} else if (TotalBreakingLines >= 4) {
			AudioManager.Instance.PlaySound (lineClear4);
			if (TotalBreakingLines == 4) {
				// ScoreManager.Instance.AddScore (100+ (placingShapeBlockCount * scoreMultiplyier));
			} else {
				// ScoreManager.Instance.AddScore ((300 + TotalBreakingLines)+ (placingShapeBlockCount * scoreMultiplyier));
			}
		}

		yield return 0;
		if (breakingRows.Count > 0) {
			foreach(List<Block> thisLine in breakingRows) {
				StartCoroutine(BreakThisLine (thisLine));
				yield return 0;				
			}	
		}
		yield return 0;		
		if (breakingRows.Count > 0) {
			yield return new WaitForSeconds (0.01F);
		}

	if (breakingColumns.Count > 0) {
			foreach(List<Block> thisLine in breakingColumns){
				StartCoroutine(BreakThisLine (thisLine));
				yield return 0;				
			}	
		}
		yield return 0;

		BlockShapeSpawner.Instance.FillShapeContainer ();
		yield return 0;

		Debug.Log("Breaking Ends ");
		isBreaking=false;
		#region time mode
		if(GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE){
			timeSlider.AddSeconds(TotalBreakingLines * 5);
		}		
		#endregion
	}

	/// <summary>
	/// Breaks the this line.
	/// </summary>
	/// <returns>The this line.</returns>
	/// <param name="breakingLine">Breaking line.</param>
	IEnumerator BreakThisLine(List<Block> breakingLine)
	{
		foreach (Block b in breakingLine) {
			b.ClearBlock ();			
			yield return new WaitForSeconds (0.001F);
		}		
		yield return 0;
	}

	/// <summary>
	/// Determines whether this instance can existing blocks placed the specified OnBoardBlockShapes.
	/// </summary>
	/// <returns><c>true</c> if this instance can existing blocks placed the specified OnBoardBlockShapes; otherwise, <c>false</c>.</returns>
	/// <param name="OnBoardBlockShapes">On board block shapes.</param>
	/*public bool CanExistingBlocksPlaced(List<ShapeInfo> OnBoardBlockShapes)
	{
		// bool canBePlace=false;				
		foreach (Block block in blockGrid) {
			if (!block.isFilled) {
				foreach (ShapeInfo info in OnBoardBlockShapes) {
					// canBePlace=false;					
					bool canPlace = CheckShapeCanPlace (block, info);					
					if (canPlace) {
						//canBePlace=true;
						return true;
					}else{
						// Debug.Log("Can't Place "+info.ShapeID);
					}					
				}
			}
		}
		return false;
	}*/

	public bool CanExistingBlocksPlaced(List<ShapeInfo> OnBoardBlockShapes)
	{
		foreach (ShapeInfo info in OnBoardBlockShapes) {				
			foreach (Block block in blockGrid) {
				if (!block.isFilled) {						
					bool canPlace = CheckShapeCanPlace (block, info);					
					if (canPlace) {						
						return true;
					}					
				}
			}			
		}
		return false;
	}
	public void DeactivateCantBePlacedBlock(List<ShapeInfo> OnBoardBlockShapes)
	{
		foreach (ShapeInfo info in OnBoardBlockShapes) {		
			bool canBePlace=false;				
			foreach (Block block in blockGrid) {
				if (!block.isFilled) {					
					bool canPlace = CheckShapeCanPlace (block, info);					
					if (canPlace) {
						canBePlace=true;
						break;						
					}					
				}
			}
			if(canBePlace){
				info.rayCaster.enabled=true;
				foreach(Transform block in info.transform){
					block.gameObject.GetComponent<Image>().color=new Color32(255,255,255,255);
				}
			}else{
				info.rayCaster.enabled=false;
				foreach(Transform block in info.transform){
					block.gameObject.GetComponent<Image>().color=new Color32(111,111,111,255);
				}
				// Debug.Log("Can't place "+info.name);			
			}
		}		
	}

	/// <summary>
	/// Checks the shape can place.
	/// </summary>
	/// <returns><c>true</c>, if shape can place was checked, <c>false</c> otherwise.</returns>
	/// <param name="placingBlock">Placing block.</param>
	/// <param name="placingBlockShape">Placing block shape.</param>
	bool CheckShapeCanPlace(Block placingBlock, ShapeInfo placingBlockShape)
	{
		int currentRowID = placingBlock.rowID;
		int currentColumnID = placingBlock.columnID;

		if(powerShape != null) 
		{
			return true;
		}
			

		if (placingBlockShape != null && placingBlockShape.ShapeBlocks != null) {
			foreach (ShapeBlock c in placingBlockShape.ShapeBlocks) {
				Block checkingCell = blockGrid.Find (o => o.rowID == currentRowID + (c.rowID + placingBlockShape.startOffsetX) && o.columnID == currentColumnID + (c.columnID - placingBlockShape.startOffsetY));
				
				if ((checkingCell == null) || (checkingCell != null && checkingCell.isFilled)) {					
					return false;
				}
			}
		}
		return true;
	}

	/// <summary>
	/// Raises the unable to place shape event.
	/// </summary>
	public void OnUnableToPlaceShape()
	{
		if (TotalRescueDone < MaxAllowedRescuePerGame) {
			GamePlayUI.Instance.ShowRescue (GameOverReason.OUT_OF_MOVES);
		} 
		else 
		{
			Debug.Log("GameOver Called..");
			StartCoroutine(GamePlayUI.Instance.ShowGameOverText());
			Invoke("OnGameOver",3f);
		}
	}

	/// <summary>
	/// Raises the bomb counter over event.
	/// </summary>
	public void OnBombCounterOver(){
		if ((TotalRescueDone < MaxAllowedRescuePerGame) || MaxAllowedRescuePerGame < 0) {
			GamePlayUI.Instance.ShowRescue (GameOverReason.BOMB_COUNTER_ZERO);
		} else {
			Debug.Log("GameOver Called..");
			OnGameOver ();
		}
	}

 public void ExecuteRescueGameOver()
    {
        if (GamePlayUI.Instance.currentGameOverReson == GameOverReason.OUT_OF_MOVES)
        {
            int TotalBreakingColumns = 8;
            int TotalBreakingRows = 8;

            int totalColumns = GameBoardGenerator.Instance.TotalColumns;
            int totalRows = GameBoardGenerator.Instance.TotalRows;


            // int startingColumn = (int)((totalColumns / 2F) - (TotalBreakingColumns / 2F));
            // int startingRow = (int)((totalRows / 2F) - (TotalBreakingRows / 2F));

            int startingColumn = 0;
            int startingRow = 0;

            List<List<Block>> breakingColums = new List<List<Block>>();

            for (int columnIndex = startingColumn; columnIndex <= (startingColumn + (TotalBreakingColumns - 1)); columnIndex++)
            {
                breakingColums.Add(GetEntireColumnForRescue(columnIndex));
            }

            List<List<Block>> breakingRows = new List<List<Block>>();

            for (int rowIndex = startingRow; rowIndex <= (startingRow + (TotalBreakingRows - 1)); rowIndex++)
            {
                breakingRows.Add(GetEntireRowForRescue(rowIndex));
            }
            StartCoroutine(BreakAllCompletedLinesRescue(breakingRows, breakingColums, 0));
        }

        #region bomb mode
        if ((GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE) && GamePlayUI.Instance.currentGameOverReson == GameOverReason.BOMB_COUNTER_ZERO)
        {
            List<Block> bombBlocks = blockGrid.FindAll(o => o.isBomb);
            foreach (Block block in bombBlocks)
            {
                if (block.bombCounter <= 1)
                {
                    block.SetCounter(block.bombCounter + 4);
                }
                block.DecreaseCounter();
            }
        }
        #endregion

        #region time mode
        if (GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE)
        {
            if (GamePlayUI.Instance.currentGameOverReson == GameOverReason.TIME_OVER)
            {
                timeSlider.AddSeconds(30);
            }
            timeSlider.ResumeTimer();
        }
        #endregion
    }
	/// <summary>
	/// Executes the rescue.
	/// </summary>
	 public void ExecuteRescue(int power = 1)
    {
        if (GamePlayUI.Instance.currentGameOverReson == GameOverReason.OUT_OF_MOVES)
        {
            int TotalBreakingColumns = 0;
            int TotalBreakingRows = 8;

            int totalColumns = GameBoardGenerator.Instance.TotalColumns;
            int totalRows = GameBoardGenerator.Instance.TotalRows;

            // int startingColumn = (int)((totalColumns / 2F) - (TotalBreakingColumns / 2F));
            // int startingRow = (int)((totalRows / 2F) - (TotalBreakingRows / 2F));

            int startingColumn = 0;
            int startingRow = 0;

            if (power == 0)
            {
                TotalBreakingColumns = 3;
                TotalBreakingRows = 3;

                // startingColumn = (int)((totalColumns / 2F) - (TotalBreakingColumns / 2F));
                // startingRow = (int)((totalRows / 2F) - (TotalBreakingRows / 2F));

				int minRowID = powerPlaceBlocks.ElementAt(0).rowID;
				int minColID = powerPlaceBlocks.ElementAt(0).columnID;

				foreach(Block b in powerPlaceBlocks){
					if(b.rowID < minRowID){
						minRowID = b.rowID;
					}

					if(b.columnID < minColID ){
						minColID = b.columnID;
					}
				}

				startingRow = minRowID;
				startingColumn = minColID;
            }

            List<List<Block>> breakingColums = new List<List<Block>>();

            for (int columnIndex = startingColumn; columnIndex <= (startingColumn + (TotalBreakingColumns - 1)); columnIndex++)
            {
                breakingColums.Add(GetEntireColumnForRescue(columnIndex));
            }

            List<List<Block>> breakingRows = new List<List<Block>>();

            for (int rowIndex = startingRow; rowIndex <= (startingRow + (TotalBreakingRows - 1)); rowIndex++)
            {
                breakingRows.Add(GetEntireRowForRescue(rowIndex));
            }
            StartCoroutine(BreakAllCompletedLinesRescue(breakingRows, breakingColums, 0));
        }
	}
    /// <summary>
    /// Ises the free rescue available.
    /// </summary>
    /// <returns><c>true</c>, if free rescue available was ised, <c>false</c> otherwise.</returns>
	public bool isFreeRescueAvailable()
	{
		if((TotalFreeRescueDone < MaxAllowedVideoWatchRescue) || (MaxAllowedVideoWatchRescue < 0))
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// Raises the rescue done event.
	/// </summary>
	/// <param name="isFreeRescue">If set to <c>true</c> is free rescue.</param>
	public void OnRescueDone(bool isFreeRescue) 
	{
		if (isFreeRescue) {
			TotalFreeRescueDone += 1;
		} else {
			TotalRescueDone += 1;
		}
		//CloseRescuePopup ();
		Invoke ("ExecuteRescueGameOver", 0.5F);
	}

	/// <summary>
	/// Raises the rescue discarded event.
	/// </summary>
	public void OnRescueDiscarded()
	{
		//CloseRescuePopup ();
		Debug.Log("GameOver Called..");
		OnGameOver ();
	}

	/// <summary>
	/// Closes the rescue popup.
	/// </summary>
	// void CloseRescuePopup()
	// {
	// 	GameObject currentWindow = StackManager.Instance.WindowStack.Peek ();
	// 	if (currentWindow != null) {
	// 		if (currentWindow.OnWindowRemove () == false) {
	// 			Destroy (currentWindow);
	// 		}
	// 		StackManager.Instance.WindowStack.Pop ();
	// 	}
	// }

	/// <summary>
	/// Raises the game over event.
	/// </summary>
	public void OnGameOver()
	{		
		GameObject gameOverScreen = StackManager.Instance.gameOverScreen;
		gameOverScreen.Activate();		
		gameOverScreen.GetComponent<GameOver> ().SetLevelScore (ScoreManager.Instance.Score, 0);
		GameProgressManager.Instance.ClearProgress();	
		StackManager.Instance.DeactivateGamePlay();
		InterstitialAdd.instance.RequestInterstitial ();
		Debug.Log ("Interstitial Add Called ");
	} 

	#region Bomb Mode Specific
	/// <summary>
	/// Updates the block count.
	/// </summary>
	void UpdateBlockCount()
	{
		List<Block> bombBlocks = blockGrid.FindAll (o => o.isBomb);
		foreach (Block block in bombBlocks) {
			block.DecreaseCounter ();
		}

		bool doPlaceBomb = false;
		if (MoveCount <= 10) {
			if (MoveCount % 5 == 0) {
				doPlaceBomb = true;
			}
		} else if (MoveCount <= 30) {
			if (((MoveCount-10) % 4 == 0)) {
				doPlaceBomb = true;
			}

		} else if (MoveCount <= 48) {
			if (((MoveCount-30) % 3 == 0)) {
				doPlaceBomb = true;
			}
		} else {
			if (((MoveCount-48) % 2 == 0)) {
				doPlaceBomb = true;
			}
		}

		if (doPlaceBomb) {
			PlaceAtRandomPlace ();
		}
	}

	/// <summary>
	/// Places at random place.
	/// </summary>
	void PlaceAtRandomPlace()
	{
		List<int> BombPlacingRows = new List<int> ();

		for (int rowIndex = 0; rowIndex < GameBoardGenerator.Instance.TotalRows; rowIndex++) {
			bool canPlace = CheckRowForBombPlace (rowIndex);
			if (canPlace) {
				BombPlacingRows.Add (rowIndex);
			}
		}

		int RandomRowForPlacingBomb = 0;
		if (BombPlacingRows.Count > 0) {
			RandomRowForPlacingBomb = BombPlacingRows [UnityEngine.Random.Range (0, BombPlacingRows.Count)];
		} else {
			RandomRowForPlacingBomb = UnityEngine.Random.Range (0, GameBoardGenerator.Instance.TotalRows);
		}

		PlaceBombAtRow (RandomRowForPlacingBomb);
	}

	/// <summary>
	/// Places the bomb at row.
	/// </summary>
	/// <param name="rowIndex">Row index.</param>
	void PlaceBombAtRow(int rowIndex)
	{
		List<Block> emptyBlockForThisRow = blockGrid.FindAll (o => o.rowID == rowIndex && o.isFilled == false); 
		Block randomBlock = emptyBlockForThisRow [UnityEngine.Random.Range (0, emptyBlockForThisRow.Count)];

		if (randomBlock != null) {
			randomBlock.ConvertToBomb ();
		}
	}

	/// <summary>
	/// Checks the row for bomb place.
	/// </summary>
	/// <returns><c>true</c>, if row for bomb place was checked, <c>false</c> otherwise.</returns>
	/// <param name="rowIndex">Row index.</param>
	bool CheckRowForBombPlace(int rowIndex)
	{
		Block block = blockGrid.Find (o => o.rowID == rowIndex && o.isFilled == true);
		int totalEmptyBlocks = blockGrid.FindAll (o => o.rowID == rowIndex && o.isFilled == false).Count;

		if (block != null && totalEmptyBlocks >= 2) {
			return true;
		} else {
			return false;
		}
	}
	#endregion


	#region show help if mode opened first time
	/// <summary>
	/// Checks for help.
	/// </summary>
	public void CheckForHelp()
	{
		bool isHelpShown = false;
		if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.ADVANCE || GameController.gameMode == GameMode.TIMED) {
			isHelpShown = PlayerPrefs.GetInt ("isHelpShown_" + GameController.gameMode.ToString (), 0) == 0 ? false : true;
			if (!isHelpShown) {
				InGameHelp inGameHelp = gameObject.AddComponent<InGameHelp> ();
				inGameHelp.StartHelp ();
			} else {
				ShowBasicHelp ();
			}
		} else {
			ShowBasicHelp ();
		}
	}

	/// <summary>
	/// Raises the help popup closed event.
	/// </summary>
	public void OnHelpPopupClosed()
	{
		if (GameController.gameMode == GameMode.TIMED) {
			timeSlider.ResumeTimer ();
		} 
		ShowBasicHelp ();
	}

	/// <summary>
	/// Shows the basic help.
	/// </summary>
	void ShowBasicHelp()
	{
		bool isBasicHelpShown = PlayerPrefs.GetInt ("isBasicHelpShown", 0) == 0 ? false : true;
		if (!isBasicHelpShown) {
			InGameHelp inGameHelp = gameObject.GetComponent<InGameHelp> ();
			if (inGameHelp == null) {
				inGameHelp = gameObject.AddComponent<InGameHelp> ();
			}
			inGameHelp.ShowBasicHelp ();
		}
	}
	#endregion
	
  public void OnRainbowBall()
    {
        if (PowerManager.Instance.IsRainbowBallAvailable())
        {
            PowerManager.Instance.UseRainbowBall();
            UpdateRainbowBallCounter();
        }

        else
        {
            //Activate Power Shop
            // StackManager.Instance.DeactivateGamePlay();
            StackManager.Instance.powerScreen.SetActive(true);

        }
    }

    public void OnMagicCube()
    {
        if (PowerManager.Instance.IsMagicCubeAvailable())
        {
			powerShape = Instantiate(MagicCube,transform.position,Quaternion.identity,MagicCube.transform.parent);
			powerShape.gameObject.SetActive(true);
			UIUpdateOnMagicCube();
        }

        else
        {
            //Activate Power Shop
            StackManager.Instance.powerScreen.SetActive(true);
        }


    }


	public void UpdateMagicCubeCounter()
    {
        MagicCubeButton.GetComponentInChildren<Text>().text = PowerManager.Instance.GetMagicCubeBalance().ToString();
    }

	public void UIUpdateOnMagicCube(){  
			
			BlockShapePanel.gameObject.SetActive(BlockShapePanel.gameObject.activeSelf^true);
			//MagicCubeCancelButton.gameObject.SetActive(MagicCubeButton.gameObject.activeSelf^true);
			MagicCubeButton.interactable = !MagicCubeButton.interactable;
			MagicCubeCancelButton.gameObject.SetActive(MagicCubeCancelButton.gameObject.activeSelf^true);
				
	}

	public void OnMagicCubeCancel(){
		Destroy(powerShape.gameObject);
		UIUpdateOnMagicCube();
	}		

    public void UpdateRainbowBallCounter()
    {
        RainbowBallButton.GetComponentInChildren<Text>().text = PowerManager.Instance.GetRainbowBallBalance().ToString();
    }


}