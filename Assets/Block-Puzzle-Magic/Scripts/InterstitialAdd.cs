﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class InterstitialAdd : MonoBehaviour
{

    public static InterstitialAdd instance;
    public InterstitialAd interstitialview;

    AdRequest request;
    public void Awake()
    {
        instance = this;
    }
    public void Start()
    {
         //string appId = "ca-app-pub-3940256099942544~3347511713"; // Test App ID
        string appId = "ca-app-pub-2238709657156519~9081346890"; //Live App ID
        MobileAds.Initialize(appId);
         //string adUnitId = "ca-app-pub-3940256099942544/1033173712";		// Test ID
       string adUnitId = "ca-app-pub-2238709657156519/7740345060"; // Live ID
        interstitialview = new InterstitialAd(adUnitId);
        request = new AdRequest.Builder().Build();
        interstitialview.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        interstitialview.LoadAd(request);
    }
    public void RequestInterstitial()
    {
        Debug.Log("RequestInterstial ");
        if (interstitialview.IsLoaded())
        {
            interstitialview.Show();
            StartCoroutine(LoadAdAfterSeconds(2));
        }
        else
        {
            interstitialview.LoadAd(request);
        }
    }

    private IEnumerator LoadAdAfterSeconds(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        interstitialview.LoadAd(request);
        yield return 0;
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("Interstitial failed to load: " + args.Message);
        // Handle the ad failed to load event.
    }
}