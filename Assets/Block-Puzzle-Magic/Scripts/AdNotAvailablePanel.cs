﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdNotAvailablePanel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnOkayPressed()
    {
        if (InputManager.Instance.canInput())
        {
			Debug.Log("Okay Clicked");
            AudioManager.Instance.PlayButtonClickSound();
            StackManager.Instance.shopScreen.Activate();
            // GamePlay.Instance.OnGameOver();
            gameObject.SetActive(false);
        }
    }
}
