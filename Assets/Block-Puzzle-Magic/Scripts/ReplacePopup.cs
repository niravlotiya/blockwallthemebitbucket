﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplacePopup : MonoBehaviour {

	public Transform newShape;
    public Transform currentShape;

	// Use this for initialization
	void Start () {
        BlockShapeSpawner.Instance.CreateShapeWithID(currentShape, ShapeReplaceManager.currentShapeId);
		BlockShapeSpawner.Instance.CreateShapeWithID(newShape, ShapeReplaceManager.replacementShapeId);
		StartCoroutine(WaitAndDisable());
	}
	
	IEnumerator WaitAndDisable()
	{
		yield return new WaitForSeconds(1.5F);
		gameObject.Deactivate();
	}
}
