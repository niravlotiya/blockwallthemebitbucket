﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GamePlayUI : Singleton<GamePlayUI> 
{
	[SerializeField] private GameObject alertWindow;
	Text txtAlertText;
	public RectTransform gameOverText;

	public AnimationCurve animationCurve;

	public GameOverReason currentGameOverReson;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		txtAlertText = alertWindow.transform.GetChild (0).GetComponentInChildren<Text> ();
	}

	public void OnPauseButtonPressed(){
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
			StackManager.Instance.pauseSceen.SetActive(true);
		}
	}

	public void OnPowerButtonPressed()
	{
		if (InputManager.Instance.canInput ()) 
		{
			AudioManager.Instance.PlayButtonClickSound ();
		}
	}

	public void ShowAlert()
	{
		alertWindow.SetActive (true);
		if (!IsInvoking ("CloseAlert")) {
			Invoke ("CloseAlert", 2F);
		}
	}

	/// <summary>
	/// Closes the alert.
	/// </summary>
	void CloseAlert()
	{
		alertWindow.SetActive (false);
	}

	/// <summary>
	/// Shows the rescue.
	/// </summary>
	/// <param name="reason">Reason.</param>
	public void ShowRescue(GameOverReason reason)
	{
		currentGameOverReson = reason;
		StartCoroutine (ShowRescueScreen(reason));
	}

	/// <summary>
	/// Shows the rescue screen.
	/// </summary>
	/// <returns>The rescue screen.</returns>
	/// <param name="reason">Reason.</param>
	IEnumerator ShowRescueScreen(GameOverReason reason)
	{		
		#region time mode
		if(GameController.gameMode == GameMode.TIMED || GameController.gameMode == GameMode.CHALLENGE){
			GamePlay.Instance.timeSlider.PauseTimer();
		}
		#endregion

		switch (reason) {
		case GameOverReason.OUT_OF_MOVES:
			// txtAlertText.SetLocalizedTextForTag ("txt-out-moves");
			break;
		case GameOverReason.BOMB_COUNTER_ZERO:
			txtAlertText.SetLocalizedTextForTag ("txt-bomb-blast");
			break;
		case GameOverReason.TIME_OVER:
			txtAlertText.SetLocalizedTextForTag ("txt-time-over");
			break;
		}

		yield return new WaitForSeconds (0.5F);
		alertWindow.SetActive (true);
		yield return new WaitForSeconds (1.5F);
		alertWindow.SetActive (false);
		StackManager.Instance.recueScreen.Activate();
	}
	public IEnumerator ShowGameOverText(){		
		gameOverText.gameObject.SetActive(true);
		float t = 0.0f;
		Vector3 endPosition=new Vector3(0,0,0);
		Vector3 startPosition=new Vector3(0,-600,0);
		
		while (t < 1)
        {
			// Debug.Log("ShowGameOverText "+t);
            t += Time.deltaTime /1.5f;  
			gameOverText.localPosition=Vector3.Lerp(startPosition,endPosition,animationCurve.Evaluate(t));          			
            yield return 0;
        }
		yield return new WaitForSeconds(2f);
		gameOverText.gameObject.SetActive(false);
		yield return 0;
	}
}

/// <summary>
/// Game over reason.
/// </summary>
public enum GameOverReason
{
	OUT_OF_MOVES = 0,
	BOMB_COUNTER_ZERO = 1,
	TIME_OVER
}