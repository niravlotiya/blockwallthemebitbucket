﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

#if HBDOTween
using DG.Tweening;
#endif


public class BlockShapeSpawner : Singleton<BlockShapeSpawner> 
{
	[Tooltip("Setting this true means placing a block will add new block instantly, false means new shape blocks will be added only once all three are placed on the board.")]
	public bool keepFilledAlways = false;

	[SerializeField] ShapeBlockList shapeBlockList;

	[SerializeField] ShapeBlockList shapeBlockList_Plus;

	[SerializeField] public ShapeBlockList[] shapeBlockListNew;


	//[HideInInspector] 
	public ShapeBlockList ActiveShapeBlockModule;

	[SerializeField] public Transform[] ShapeContainers;

	public List<int> shapeBlockProbabilityPool;

	int shapeBlockPoolCount = 1;

	public int levelNo=0;
	public static BlockShapeSpawner Instance;
	public List<ShapeInfo> testShapes;
	public List<int> listOfUndoedShapes;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake()
	{
		/*if (GameController.gameMode == GameMode.ADVANCE || GameController.gameMode == GameMode.CHALLENGE) {
			ActiveShapeBlockModule = shapeBlockList_Plus;
		} else {
			ActiveShapeBlockModule= shapeBlockList;
		}*/
		Instance = this;
		ActiveShapeBlockModule = shapeBlockList_Plus;
		testShapes = new List<ShapeInfo>();
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		#region blast mode
		if (GameController.gameMode == GameMode.BLAST || GameController.gameMode == GameMode.CHALLENGE) {
			keepFilledAlways = true;
		}
		#endregion

		Invoke ("SetupPreviousSessionShapes", 0.2F);
		Invoke ("createShapeBlockProbabilityList", 0.5F);
		Invoke ("FillShapeContainer", 0.5F);
	}

	/// <summary>
	/// Setups the previous session shapes.
	/// </summary>
	public void SetupPreviousSessionShapes()
	{
		if (GameBoardGenerator.Instance.previousSessionData != null) {
			List<int> shapes = GameBoardGenerator.Instance.previousSessionData.shapeInfo.Split(',').Select(Int32.Parse).ToList();

			int shapeIndex = 0;
			foreach (int shapeID in shapes) {
				if (shapeID >= 0) {
					CreateShapeWithID (ShapeContainers [shapeIndex], shapeID);
				}
				shapeIndex += 1;
			}
		}
	}

	public void UndoShapes(PreviousSessionData previousSessionData){
		if (previousSessionData != null) {
			List<int> shapes = previousSessionData.shapeInfo.Split(',').Select(Int32.Parse).ToList();
			int countOfFilledShapes = 0;

			int shapeIndex = 0;
			foreach (int shapeID in shapes) {
				if (shapeID >= 0) {
					CreateShapeWithID (ShapeContainers [shapeIndex], shapeID);
					countOfFilledShapes++;
				}
				shapeIndex += 1;
			}

			if(countOfFilledShapes==1)
			{
				SaveUndoedShapesForRegeneration();
			}
		}
	}

	private void SaveUndoedShapesForRegeneration()
	{
		Debug.Log("undoed shapes saved");
		List<int> shapes = GetAllOnBoardShapeNames().Split(',').Select(Int32.Parse).ToList();
		listOfUndoedShapes.InsertRange(0,shapes);
	}

	/// <summary>
	/// Creates the shape block probability list.
	/// </summary>
	public void createShapeBlockProbabilityList()
	{		
		// Debug.Log("Create shape block probability list");
		if(levelNo==0 || ScoreManager.Instance.GetScore()<=1000){
			levelNo=1;
			Debug.Log(" level is 1");
		}else if(ScoreManager.Instance.GetScore()>1000 && ScoreManager.Instance.GetScore()<=2000){
			levelNo=2;
			Debug.Log(" level is 2");
		}else if(ScoreManager.Instance.GetScore()>2000 && ScoreManager.Instance.GetScore()<=3000){
			levelNo=3;		
			Debug.Log(" level is 3");	
		}
		else if(ScoreManager.Instance.GetScore()>3000 && ScoreManager.Instance.GetScore()<=4000){
			levelNo=4;	
			Debug.Log(" level is 4");		
		}else{
			levelNo=5;
			Debug.Log(" level is 5");
		}

		shapeBlockProbabilityPool = new List<int> ();
		if (ActiveShapeBlockModule != null) {
			/*foreach (ShapeBlockSpawn shapeBlock in ActiveShapeBlockModule.ShapeBlocks) {
				AddShapeInProbabilityPool (shapeBlock.BlockID, shapeBlock.spawnProbability);
			}*/		
			for(int i=0;i<levelNo && shapeBlockListNew.Length>=levelNo;i++){				
				foreach(ShapeBlockSpawn shapeBlock in shapeBlockListNew[i].ShapeBlocks){
					AddShapeInProbabilityPool (shapeBlock.BlockID, shapeBlock.spawnProbability);				
				}
			}			
		}
		
		// Debug.Log("createShapeBlockProbabilityList "+shapeBlockProbabilityPool.Count);
		shapeBlockProbabilityPool.Shuffle ();
	}

	/// <summary>
	/// Adds the shape in probability pool.
	/// </summary>
	/// <param name="blockID">Block I.</param>
	/// <param name="probability">Probability.</param>
	void AddShapeInProbabilityPool(int blockID, int probability) {
		int probabiltyTimesToAdd = shapeBlockPoolCount * probability;

		for (int index = 0; index < probabiltyTimesToAdd; index++) {
			shapeBlockProbabilityPool.Add (blockID);
		}
	}

	/// <summary>
	/// Fills the shape container.
	/// </summary>
	public void FillShapeContainer()
	{
		// ReorderShapes ();

		if (!keepFilledAlways) {
			bool isAllEmpty = true;
			foreach (Transform shapeContainer in ShapeContainers) {
				if (shapeContainer.childCount > 0) {
					isAllEmpty = false;
				}
			}

			if (isAllEmpty) {
				if(listOfUndoedShapes != null && listOfUndoedShapes.Count > 0)
				{
					GameProgressManager.countOfMovesAfterNewShapesGenerated = 0;
					foreach (Transform shapeContainer in ShapeContainers) {
						int id = listOfUndoedShapes[0];
						listOfUndoedShapes.RemoveAt(0);
						CreateShapeWithID (shapeContainer,id);
					}
				}
				else
				{
					GameProgressManager.countOfMovesAfterNewShapesGenerated = 0;
					foreach (Transform shapeContainer in ShapeContainers) {
						AddRandomShapeToContainer (shapeContainer);
					}
				}
			}
		} else {
			foreach (Transform shapeContainer in ShapeContainers) {
				if (shapeContainer.childCount <= 0) {
					AddRandomShapeToContainer (shapeContainer);
				}
			}
		}

		Invoke ("CheckOnBoardShapeStatus", 0.2F);
	}

	/// <summary>
	/// Adds the random shape to container.
	/// </summary>
	/// <param name="shapeContainer">Shape container.</param>
	public void AddRandomShapeToContainer(Transform shapeContainer)
	{
		if (shapeBlockProbabilityPool == null || shapeBlockProbabilityPool.Count <= 0) {
			Debug.Log("createShapeBlockProbability");
			createShapeBlockProbabilityList ();
		}
		else{
			Debug.Log( "shapeblockcount" + shapeBlockProbabilityPool.Count);
		}

		int RandomShape = shapeBlockProbabilityPool [0];
		shapeBlockProbabilityPool.RemoveAt (0);

		GameObject newShapeBlock = ActiveShapeBlockModule.ShapeBlocks.Find(o => o.BlockID == RandomShape).shapeBlock;
		GameObject spawningShapeBlock = (GameObject)(Instantiate (newShapeBlock)) as GameObject;
		spawningShapeBlock.transform.SetParent (shapeContainer);
		spawningShapeBlock.transform.localScale = Vector3.one * 0.5F;
		spawningShapeBlock.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (800F, 0, 0);
		#if HBDOTween
		// spawningShapeBlock.transform.DOLocalMove (Vector3.zero, 0.05F);
		#endif
		spawningShapeBlock.transform.localPosition=Vector3.zero;
	}

	/// <summary>
	/// Creates the shape with I.
	/// </summary>
	/// <param name="shapeContainer">Shape container.</param>
	/// <param name="shapeID">Shape I.</param>
	public GameObject CreateShapeWithID(Transform shapeContainer, int shapeID)
	{
		GameObject newShapeBlock = ActiveShapeBlockModule.ShapeBlocks.Find(o => o.BlockID == shapeID).shapeBlock;
		GameObject spawningShapeBlock = (GameObject)(Instantiate (newShapeBlock)) as GameObject;
		spawningShapeBlock.transform.SetParent (shapeContainer);
		spawningShapeBlock.transform.localScale = Vector3.one * 0.5F;
		spawningShapeBlock.GetComponent<RectTransform> ().anchoredPosition3D = new Vector3 (800F, 0, 0);
		#if HBDOTween
		// spawningShapeBlock.transform.DOLocalMove (Vector3.zero, 0.05F);		
		#endif
		spawningShapeBlock.transform.localPosition=Vector3.zero;
		return spawningShapeBlock;		
	}

	/// <summary>
	/// Checks the on board shape status.
	/// </summary>
	public void CheckOnBoardShapeStatus()
	{
		List<ShapeInfo> OnBoardBlockShapes = new List<ShapeInfo> ();
		foreach (Transform shapeContainer in ShapeContainers) {
			if (shapeContainer.childCount > 0) {
				OnBoardBlockShapes.Add (shapeContainer.GetChild (0).GetComponent<ShapeInfo> ());
				
				ShapeInfo lastShape =  OnBoardBlockShapes[OnBoardBlockShapes.Count-1];
				int id = lastShape.rotatedShapeID;

				while(id != lastShape.ShapeID)
				{
					GameObject temporaryShape = CreateShapeWithID(shapeContainer,id);
					ShapeInfo shape = temporaryShape.GetComponent<ShapeInfo>();
					shape.rayCaster.enabled = false;
					temporaryShape.GetComponent<Canvas>().enabled = false;
					testShapes.Add(shape);
					OnBoardBlockShapes.Add(shape);
					id = temporaryShape.GetComponent<ShapeInfo>().rotatedShapeID;
				}
			}
		}

		StartCoroutine(WaitAndTest(OnBoardBlockShapes));
	}

	IEnumerator WaitAndTest(List<ShapeInfo> OnBoardBlockShapes)
	{
		yield return new WaitForSeconds(0.1F);
		bool canExistingBlocksPlaced = GamePlay.Instance.CanExistingBlocksPlaced (OnBoardBlockShapes);
		GamePlay.Instance.DeactivateCantBePlacedBlock (OnBoardBlockShapes);
		DestroyTestShapes(OnBoardBlockShapes);
		
		if (canExistingBlocksPlaced == false) {
			GamePlay.Instance.OnUnableToPlaceShape ();
		}
		
	}

	void DestroyTestShapes(List<ShapeInfo> OnBoardBlockShapes)
	{
		int length = testShapes.Count;

		for(int index = 0 ; index < length ; index++)
		{
			OnBoardBlockShapes.Remove(testShapes[0]);
			GameObject temp = testShapes[0].gameObject;
			testShapes.RemoveAt(0);
			Destroy(temp);
		}

	}

	/// <summary>
	/// Reorders the shapes.
	/// </summary>
	void ReorderShapes()
	{
		List<Transform> EmptyShapes = new List<Transform> ();

		foreach (Transform shapeContainer in ShapeContainers) {
			if (shapeContainer.childCount == 0) {
				EmptyShapes.Add (shapeContainer);
			} else {
				if (EmptyShapes.Count > 0) {
					Transform emptyContainer = EmptyShapes [0];
					shapeContainer.GetChild (0).SetParent (emptyContainer);
					EmptyShapes.RemoveAt (0);
					#if HBDOTween
					// emptyContainer.GetChild (0).DOLocalMove (Vector3.zero, 0.05F);
					#endif
					emptyContainer.GetChild(0).localPosition=Vector3.zero;
					EmptyShapes.Add (shapeContainer);
				}
			}
		}
	}

	/// <summary>
	/// Gets all on board shape names.
	/// </summary>
	/// <returns>The all on board shape names.</returns>
	public string GetAllOnBoardShapeNames()
	{
		string shapeNames = "";
		foreach (Transform shapeContainer in ShapeContainers) {
			if (shapeContainer.childCount > 0) {
				shapeNames = shapeNames + shapeContainer.GetChild (0).GetComponent<ShapeInfo> ().ShapeID + ",";
			} else {
				shapeNames = shapeNames + "-1,";
			}
		}

		shapeNames = shapeNames.Remove (shapeNames.Length-1);
		return shapeNames;
	}

	public void ClearShapeContainers(){
		foreach(Transform t in ShapeContainers){
			if(t.childCount>0){
				Destroy(t.GetChild(0).gameObject);
			}
		}
	}

	public IEnumerator WaitAndCheckOnBoardShapeStatus()
	{
		yield return new WaitForSeconds(0.1F);
		CheckOnBoardShapeStatus();
	}
}

