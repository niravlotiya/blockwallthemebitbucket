﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameController : Singleton<GameController> 
{
    public float currentUpdateVersion=0;
	public const string BOARD_NAME="BoardName";
    public GameObject checkForUpdatePanel;

    public string version_type;
    public string isForceUpdate;

    public string isShowUpdate;

    public string appStoreversion;

    public string popupTitle;
    public string popupMessage;
    public string yesButtonText;
    public string nobuttonText;     

	public static GameMode gameMode = GameMode.CLASSIC;
	public Canvas UICanvas; 


	// Checks if interner is available or not.
	public bool isInternetAvailable()
	{
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork) {
			return true;
		}
		return false;
	}

    void Start(){
        checkForUpdatePanel.SetActive(false);
        StartCoroutine(CallCheckForUpdatesAPI());
    }
	/// <summary>
	/// Quits the game.
	/// </summary>
	public void QuitGame()
	{
		Invoke ("QuitGameWithDelay", 0.2F);
	}

	/// <summary>
	/// Quits the game with delay.
	/// </summary>
	void QuitGameWithDelay ()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}

	#region CoRoutines
    IEnumerator CallCheckForUpdatesAPI()
    {
        WWWForm wf = new WWWForm();        
        wf.AddField("version", "android");        
        Dictionary<string,string> hashTable=new Dictionary<string,string>();
        hashTable.Add("key","xyz");
        
        WWW w = new WWW("http://adityapromotions.com/public/api/v1/version", wf.data,hashTable);
        yield return w;

        if (w.text != null && w.text != "")
        {
            JSONObject jo = new JSONObject(w.text);
            Debug.Log("Update "+jo.ToString());
            if (jo.GetField("status").b)
            {                    
                version_type=jo.GetField("version_type").str;
                isForceUpdate=jo.GetField("isForceUpdate").str;
                isShowUpdate=jo.GetField("isShowUpdate").str;
                appStoreversion=jo.GetField("appStoreversion").str;
                popupTitle=jo.GetField("popupTitle").str;
                popupMessage=jo.GetField("popupMessage").str;
                yesButtonText=jo.GetField("yesButtonText").str;
                nobuttonText=jo.GetField("nobuttonText").str;    

                if(currentUpdateVersion<float.Parse(appStoreversion)){
                    CheckForUpdates.Instance.titleText.text= popupTitle;
                    CheckForUpdates.Instance.messageText.text= popupMessage;
                    CheckForUpdates.Instance.yesButtonText.text= yesButtonText;
                    // CheckForUpdates.Instance.noButtonText.text= nobuttonText;
                    if(bool.Parse(isForceUpdate)){                        
                        global::CheckForUpdates.Instance.cancelButton.SetActive(false);
                    }else{
                        global::CheckForUpdates.Instance.cancelButton.SetActive(true);
                    }
                    checkForUpdatePanel.SetActive(true);
                }else{

                }                
            }
            else
            {
                // messageText.text = jo.GetField("error").str;
            }
        }
        else
        {
            // messageText.text = "Internet not connected";
        }
        yield return 0;
    }
    #endregion
}
