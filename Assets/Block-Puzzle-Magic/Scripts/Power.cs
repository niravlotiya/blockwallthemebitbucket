﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class Power : MonoBehaviour
{


    public int magicCubeValue = 5;
    public  int rainbowBallValue = 25;
    public int undoFunctionValue = 10;
    public int rotateBlockValue=2;

    public Text magicCubeCounter;
    public Text rainbowBallCounter;
    public Text undoCounter;
    public Text rotateCounter;

    public static Power Instance;


    public static event Action UpdateGamePlayCounters;
    // Use this for initialization
    void Start()
    {
                
    }

	private void OnEnable() {
		UpdateMagicCubeCounter();
        UpdateRainbowBallCounter();
        UpdateUndoFunctionCounter();
        UpdateRotateCounter();
	}

    // Update is called once per frame
    void Update()
    {

    }

    public void OnBuyMagicCube()
    {
        if (CurrencyManager.Instance.deductBalance(magicCubeValue))
        {
            PowerManager.Instance.AddOneMagicCube();
            //ScoreManager.Instance.txtCoins.text = CurrencyManager.Instance.GetCoinBalance().ToString();
            UpdateMagicCubeCounter();
        }

        else
        {
            //Actiavte Shop
            //Debug.Log("False");

            StackManager.Instance.powerScreen.SetActive(false);
            StackManager.Instance.shopScreen.SetActive(true);

        }
    }

public void OnBuyRotate(){
if (CurrencyManager.Instance.deductBalance(rotateBlockValue))
        {
            PowerManager.Instance.AddOneRotate();
            //ScoreManager.Instance.txtCoins.text = CurrencyManager.Instance.GetCoinBalance().ToString();
            UpdateRotateCounter();
        }

        else
        {
            //Actiavte Shop
            //Debug.Log("False");

            StackManager.Instance.powerScreen.SetActive(false);
            StackManager.Instance.shopScreen.SetActive(true);

        }

}
    public void OnBuyRainbowBall()
    {
        if (CurrencyManager.Instance.deductBalance(rainbowBallValue))
        {
            PowerManager.Instance.AddOneRainbowBall();
           // ScoreManager.Instance.txtCoins.text = CurrencyManager.Instance.GetCoinBalance().ToString();
            UpdateRainbowBallCounter();
        }

        else
        {
            StackManager.Instance.powerScreen.SetActive(false);
            StackManager.Instance.shopScreen.SetActive(true);
        }

        //gameObject.Deactivate();
    }

    public void OnBuyUndoFunction()
    {
        if (CurrencyManager.Instance.deductBalance(undoFunctionValue))
        {
            PowerManager.Instance.AddOneUndoFunction();
           // ScoreManager.Instance.txtCoins.text = CurrencyManager.Instance.GetCoinBalance().ToString();
            UpdateUndoFunctionCounter();
        }

        else
        {
            StackManager.Instance.powerScreen.SetActive(false);
            StackManager.Instance.shopScreen.SetActive(true);
        }
    }

    public void OnEarnCoin()
    {
        
           
    }

    public void OnClose()
    {
        StackManager.Instance.powerScreen.SetActive(false);
        // StackManager.Instance.ActivateGamePlay();
        //StackManager.Instance.gameplay.SetActive(true);

    }

    public void OnInformation()
    {
        StackManager.Instance.informationScreen.SetActive(true);	
    }

    public void UpdateRotateCounter(){
        rotateCounter.text=PowerManager.Instance.GetRotateBalance().ToString();
        
        if (UpdateGamePlayCounters != null)
        {
			UpdateGamePlayCounters.Invoke();
        }
    }

    public void UpdateMagicCubeCounter()
    {
        magicCubeCounter.text = PowerManager.Instance.GetMagicCubeBalance().ToString();

		if (UpdateGamePlayCounters != null)
        {
			UpdateGamePlayCounters.Invoke();
        }
    }

    private void UpdateRainbowBallCounter()
    {
        rainbowBallCounter.text = PowerManager.Instance.GetRainbowBallBalance().ToString();

        if (UpdateGamePlayCounters != null)
        {
			UpdateGamePlayCounters.Invoke();
        }
    }

    private void UpdateUndoFunctionCounter()
    {
        undoCounter.text = PowerManager.Instance.GetUndoFunctionBalance().ToString();

		if (UpdateGamePlayCounters != null)
        {
			UpdateGamePlayCounters.Invoke();
        }
    }

   
}
