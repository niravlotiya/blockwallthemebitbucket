﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PowerManager : MonoBehaviour
{
    private int magicCubeBalance;
    private int rainbowBallBalance;
    private int undoFunctionBalance;
    private int rotateFuctionBalance;

    public int InitialMagicCubeBalance = 10;
    public int InitialRainbowBallBalance = 10;
    public int InitialUndoFunctionBalance = 10;
    public int InitialRotateFunctionBalance = 100;

    public static string MAGIC_CUBE_BALANCE = "BlockWallMagicCubeBalance";
    public static string RAINBOW_BALL_BALANCE = "BlockWallRainbowBallBalance";
    public static string Undo_Function_BALANCE = "BlockWallUndoFunctionBalance";
    public static string Rotate_Function_BALANCE = "BlockWallRotateFunctionBalance";
    public static PowerManager Instance;
    void Awake()
    {
        magicCubeBalance = PlayerPrefs.GetInt(MAGIC_CUBE_BALANCE, InitialMagicCubeBalance);
        rainbowBallBalance = PlayerPrefs.GetInt(RAINBOW_BALL_BALANCE, InitialRainbowBallBalance);
        undoFunctionBalance = PlayerPrefs.GetInt(Undo_Function_BALANCE, InitialUndoFunctionBalance);
       // rotateFuctionBalance = PlayerPrefs.GetInt(Rotate_Function_BALANCE, InitialRotateFunctionBalance);
        Instance = this;
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool IsRainbowBallAvailable()
    {
        if (rainbowBallBalance > 0)
            return true;

        else
            return false;
    }

    public void UseRainbowBall()
    {
        rainbowBallBalance--;
        PlayerPrefs.SetInt(RAINBOW_BALL_BALANCE, rainbowBallBalance);
        GamePlay.Instance.ExecuteRescue(1);
    }

    public bool IsMagicCubeAvailable()
    {
        if (magicCubeBalance > 0)
            return true;

        else
            return false;
    }

    public void UseMagicCube()
    {
        magicCubeBalance--;
        PlayerPrefs.SetInt(MAGIC_CUBE_BALANCE, magicCubeBalance);
        GamePlay.Instance.ExecuteRescue(0);
    }

    public int GetRainbowBallBalance()
    {
        return rainbowBallBalance;
    }

    public int GetMagicCubeBalance()
    {
        return magicCubeBalance;
    }

    public int GetUndoFunctionBalance()
    {
        return undoFunctionBalance;
    }

    public int GetRotateBalance(){
        return rotateFuctionBalance;
    }

    public void AddOneRotate(){
       // rotateFuctionBalance++;
      //  PlayerPrefs.SetInt(Rotate_Function_BALANCE,rotateFuctionBalance);
    }

    public void AddOneMagicCube()
    {
        magicCubeBalance++;
        PlayerPrefs.SetInt(MAGIC_CUBE_BALANCE, magicCubeBalance);
    }

    public void AddOneRainbowBall()
    {
        rainbowBallBalance++;
        PlayerPrefs.SetInt(RAINBOW_BALL_BALANCE, rainbowBallBalance);
    }

    public void AddOneUndoFunction()
    {
        undoFunctionBalance++;
        PlayerPrefs.SetInt(Undo_Function_BALANCE, undoFunctionBalance);
    }

    public bool IsUndoFunctionAvailable()
    {
        if (undoFunctionBalance > 0)
            return true;

        else
            return false;
    }

    public void UseUndoFunction()
    {
        if (GameProgressManager.undoCount > 0)
        {
            undoFunctionBalance--;
            PlayerPrefs.SetInt(Undo_Function_BALANCE, undoFunctionBalance);
            GameProgressManager.Instance.OnExecuteUndo();
        }
    }

    public bool IsRotaTEFunctionAvailable()
    {
        if (rotateFuctionBalance > 0)
            return true;

        else
            return false;
    }

    public void UseRotateFunction(PointerEventData eventdata)
    {

        
        GamePlay.Instance.ClickResolver(eventdata);
      //  rotateFuctionBalance--;
       // PlayerPrefs.SetInt(Rotate_Function_BALANCE,rotateFuctionBalance);
    }


}
