﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using System;

public class PowerAd : MonoBehaviour 
{
	public static PowerAd Instance;
	public RewardBasedVideoAd rewardBasedVideo;
	public AdRequest request;

	private bool showPowerAd;

	public int powerCoins = 10;


	#if UNITY_ANDROID
             //string adUnitId = "ca-app-pub-3940256099942544/5224354917"; //Test ID
            string adUnitId="ca-app-pub-2238709657156519/6828194399"; // Live ad ID
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/1712485313";
        #else
            string adUnitId = "unexpected_platform";
        #endif

	void Awake()
	{
		Instance=this;
	} 

public void Start()
    {
        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

       // this.RequestRewardBasedVideo();
    }
	public void ShowRewardedAd()
	{
		this.rewardBasedVideo.Show();
	}

    public void PowerRequestRewardBasedVideo()
    {     
        Debug.Log(" Loading Ad");
        // Create an empty ad request.
        request = new AdRequest.Builder().Build();
        // Load the rewarded video ad with the request.
        this.rewardBasedVideo.LoadAd(request, adUnitId);
		showPowerAd = true;
		Debug.Log(" power Ad " + showPowerAd);
        StackManager.Instance.loadingPanel.SetActive(true);
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoLoaded event received Loaded");
        StackManager.Instance.loadingPanel.SetActive(false);
        ShowRewardedAd();
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);

         StackManager.Instance.loadingPanel.SetActive(false);
         StackManager.Instance.adNotAvaiable.SetActive(true);
        Rescue.Instance.btnWatchVideo.interactable=false;                    
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        Debug.Log("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {				
        //request = new AdRequest.Builder().Build();        
        //this.rewardBasedVideo.LoadAd(request, adUnitId);
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        Debug.Log("HandleRewardBasedVideoRewarded event received");
        string type = args.Type;
        double amount = args.Amount;
        showPowerAd = false;
		CurrencyManager.Instance.AddCoinBalance(powerCoins);
        StackManager.Instance.powerScreen.SetActive(true);
        gameObject.Deactivate();

    }
       // this.RequestRewardBasedVideo();        				        
    

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    public IEnumerator DisplayAdNotAvailableScreenForSeconds(int seconds)
    {
            yield return new WaitForSeconds(seconds);
    }
	
}
