﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour 
{
	// The button to turn on/off sound.
	public Button btnSound;	
	// Image of the button on which sound sprite will get assigned. Default on
	public Image btnSoundImage; 
	// Sound on sprite.
	public Sprite soundOnSprite1;

	public Sprite soundOnSprite2;
	// Sounf off sprite.
	public Sprite soundOffSprite1;

	public Sprite soundOffSprite2;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start()
	{
		btnSound.onClick.AddListener(() => 
		{
			if (InputManager.Instance.canInput ()) {
				AudioManager.Instance.PlayButtonClickSound ();
				AudioManager.Instance.ToggleSoundStatus();
			}
		});
	}

	/// <summary>
	/// Raises the enable event.
	/// </summary>
	void OnEnable()
	{

		AudioManager.OnSoundStatusChangedEvent += OnSoundStatusChanged;
		initSoundStatus ();
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable()
	{
		AudioManager.OnSoundStatusChangedEvent -= OnSoundStatusChanged;
	}

	/// <summary>
	/// Inits the sound status.
	/// </summary>
	void initSoundStatus()
	{
		Debug.Log (" nightMode On  "+ UIThemeManager.Instance.isDarkThemeEnabled);
		if (UIThemeManager.Instance.isDarkThemeEnabled) 
		{
			Debug.Log (" nightMode On  True");
			btnSoundImage.sprite = (AudioManager.Instance.isSoundEnabled) ? soundOnSprite1 : soundOffSprite1;
		}
		else 
		{
			Debug.Log (" nightMode On  false");
			btnSoundImage.sprite = (AudioManager.Instance.isSoundEnabled) ? soundOnSprite2 : soundOffSprite2;
		}
	}

	/// <summary>
	/// Raises the sound status changed event.
	/// </summary>
	/// <param name="isSoundEnabled">If set to <c>true</c> is sound enabled.</param>
	void OnSoundStatusChanged (bool isSoundEnabled)
	{
		Debug.Log (" is ThameChange " + UIThemeManager.Instance.isDarkThemeEnabled);
		if (UIThemeManager.Instance.isDarkThemeEnabled) 
		{
			btnSoundImage.sprite = (isSoundEnabled) ? soundOnSprite1 : soundOffSprite1;	
		}
		else 
		{
			btnSoundImage.sprite = (isSoundEnabled) ? soundOnSprite2 : soundOffSprite2;	
		}
	}
}
