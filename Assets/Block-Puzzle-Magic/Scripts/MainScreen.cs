﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainScreen : MonoBehaviour 
{	
	public InputField boardsName;
	/// <summary>
	/// Raises the play button pressed event.
	/// </summary>
	public void OnPlayButtonPressed()
	{
	/*	if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
			StackManager.Instance.selectModeScreen.Activate();
		}		
		*/
		OnClassicButtonPressed();
	}

	void Start(){
		if(!PlayerPrefs.GetString(GameController.BOARD_NAME,"").Equals("")){
			boardsName.text=PlayerPrefs.GetString(GameController.BOARD_NAME);
		}
	}

	public void OnClassicButtonPressed()
	{
		//Debug.Log("OnClassicButtonPressed 1");
		if (InputManager.Instance.canInput ()) {
		//	Debug.Log("OnClassicButtonPressed 2");
			AudioManager.Instance.PlayButtonClickSound ();
			GameController.gameMode = GameMode.CLASSIC;
			//StackManager.Instance.gameplay.SetActive (true);
			//StackManager.Instance.mainMenu.SetActive (false);
			StackManager.Instance.ActivateGamePlay();
			StackManager.Instance.mainMenu.Deactivate();
			gameObject.Deactivate();
		}
	}
	public void SaveBoardName(){		
		PlayerPrefs.SetString(GameController.BOARD_NAME,boardsName.text.Trim());
	}
}
	