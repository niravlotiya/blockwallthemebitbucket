﻿using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;

public class BannerAdd : MonoBehaviour
{
    public static BannerAdd instance;
    public BannerView bannerView;

    public void Awake()
    {
        instance = this;
    }
    public void Start()
    {
         //string appId = "ca-app-pub-3940256099942544~3347511713"; //Test ID
        string appId = "ca-app-pub-2238709657156519~9081346890"; //Live App ID		
        MobileAds.Initialize(appId);
    }
    public void RequestBanner()
    {
        // string adUnitId = "ca-app-pub-3940256099942544/6300978111";// Test ID
        string adUnitId = "ca-app-pub-2238709657156519/7193550150"; //Live ID
        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		//bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;		
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
        bannerView.Show();		
    }
    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("Banner failed to load: " + args.Message);
        // Handle the ad failed to load event.
    }

}

