﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rescue : MonoBehaviour
{
	public static Rescue Instance;

	public int rescueCoinAmount = 25;

	public Text rescueCoinAmountText;
	[SerializeField]
	public Button btnWatchVideo;

	public GameObject loadingPanel;
	[SerializeField]
	private Button btnCoinsRescue;

	[SerializeField]
	private Text txtRescueReason;

	[SerializeField]
	private Text TotalRescueAvailableText;
	public Text RemainingrescueText;


	void Awake()
	{
		Instance = this;
	}

	void OnEnable()
	{
		//YOUR AD NETWORK CALLBACK REGISTER. REPLACE THIS CODE WITH YOUR AD NETWORKS CALLBACK REGISTER
		//OnYourAdNetwork.OnWatchVideoSuccess += OnWatchVideoSuccess;

		rescueCoinAmountText.text = rescueCoinAmount + " Coins";

		TotalRescueAvailableText.text = "Rescue Available : " + (GamePlay.Instance.MaxAllowedRescuePerGame - GamePlay.Instance.TotalRescueDone).ToString();
		RemainingrescueText.text = " Remaining Rescue :  " + (GamePlay.Instance.MaxAllowedRescuePerGame - GamePlay.Instance.TotalRescueDone).ToString();

		switch (GamePlayUI.Instance.currentGameOverReson)
		{
		case GameOverReason.OUT_OF_MOVES:
			txtRescueReason.SetLocalizedTextForTag("txt-out-moves");
			break;
		case GameOverReason.BOMB_COUNTER_ZERO:
			txtRescueReason.SetLocalizedTextForTag("txt-bomb-blast");
			break;
		case GameOverReason.TIME_OVER:
			txtRescueReason.SetLocalizedTextForTag("txt-time-over");
			break;
		}
		EnableRescueButton();
		GameProgressManager.Instance.ResetUndoData();
	}

	private void EnableRescueButton()
	{
		if (GamePlay.Instance.TotalRescueDone < GamePlay.Instance.MaxAllowedRescuePerGame)
		{
			if (CurrencyManager.Instance.GetCoinBalance() >= rescueCoinAmount)
			{
				btnCoinsRescue.gameObject.SetActive(true);
				btnWatchVideo.gameObject.SetActive(false);
			}
			else
			{
				btnCoinsRescue.gameObject.SetActive(false);
				btnWatchVideo.gameObject.SetActive(true);
				btnWatchVideo.interactable=true;
				// RewardedAdManager.Instance.RequestRewardBasedVideo();
			}
		}
		else
		{
			GamePlay.Instance.OnGameOver();
			gameObject.SetActive(false);
		}
	}

	void OnDisable()
	{
		//YOUR AD NETWORK CALLBACK UNREGISTER. REPLACE THIS CODE WITH YOUR AD NETWORKS CALLBACK UNREGISTER
		// OnYourAdNetwork.OnWatchVideoSuccess +-= OnWatchVideoSuccess;



	}

	//THIS IS JUST A PLACE HOLDER CODE. YOU CAN REPLACE IT WITH YOUR OWN LOGIC.
	/* void OnWatchVideoSuccess(bool result)
    {
        if (result == true)
        {
            GamePlay.Instance.OnRescueDone(true);
        }
    }*/

	public void OnCloseButtonPressed()
	{
		if (InputManager.Instance.canInput())
		{
			AudioManager.Instance.PlayButtonClickSound();
			GamePlay.Instance.OnGameOver();
			gameObject.SetActive(false);
		}
	}

	public void OnRescueUsingWatchVideo()
	{
		if (InputManager.Instance.canInput())
		{
			//CALL YOUR AD NETWORK VIDEO AD HERE TO RESCUE USING WATCH VIDEO.

			//RewardedAdManager.Instance.ShowRewardedAd();

			RewardedAdManager.Instance.RequestRewardBasedVideo();
		}
	}

	public void OnRescueUsingCoins()
	{
		if (InputManager.Instance.canInput())
		{
			bool coinDeduced = CurrencyManager.Instance.deductBalance(rescueCoinAmount);
			ScoreManager.Instance.txtCoins.text = CurrencyManager.Instance.GetCoinBalance().ToString();
			if (coinDeduced)
			{
				GamePlay.Instance.OnRescueDone(false);
				gameObject.Deactivate();
			}
			else
			{
				StackManager.Instance.shopScreen.Activate();
			}
		}
	}

	public void OnPowerUpSelected(int power)
	{ 
		GamePlay.Instance.ExecuteRescue(power);
		gameObject.Deactivate();

	}


}
