﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OnBoardShapeCard : MonoBehaviour
{

    public Button button;
    //public static event Action OnCurrentShapeSelected;


    // Use this for initialization
    void Start()
    {
        button = gameObject.GetComponent<Button>();
        //OnBoardShapeClick();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable() {
        //OnCurrentShapeSelected += OnCurrentListDisable;
    }

    private void OnDisable() {
        //OnCurrentShapeSelected -= OnCurrentListDisable;
    }


    public void OnBoardShapeClick()
    {
         if (ScrollViewList.Instance.selectedCurrentButton != null)
         {
             ScrollViewList.Instance.selectedCurrentButton.interactable = true;
         }

        ShapeReplaceManager.currentShapeId = gameObject.GetComponentInChildren<ShapeInfo>().ShapeID;
      //  OnCurrentShapeSelected.Invoke();
        
        //ScrollViewList.Instance.nextStepInstruction.SetActive(true);
        ScrollViewList.Instance.selectedCurrentButton = button;
        button.interactable = false;
        //ScrollViewList.Instance.nextStepInstruction.transform.SetAsFirstSibling();
        //ScrollViewList.Instance.nextStepInstruction.transform.SetSiblingIndex(gameObject.transform.GetSiblingIndex());
    }

    public void OnCurrentListDisable()
    {//
    // Destroy(gameObject);
    }
}
