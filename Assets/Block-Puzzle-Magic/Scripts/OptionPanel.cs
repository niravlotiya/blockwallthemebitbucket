﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class OptionPanel : MonoBehaviour 
{
	public string ScreenshotName;

	/// <summary>
	/// Raises the settings button pressed event.
	/// </summary>
	public void OnSettingsButtonPressed()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
			StackManager.Instance.settingsScreen.Activate();
		}
	}

	/// <summary>
	/// Raises the messages button pressed event.
	/// </summary>
	public void OnMessagesButtonPressed()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
		}
	}

	/// <summary>
	/// Raises the rate button pressed event.
	/// </summary>
	public void OnRateButtonPressed()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();

			if (GameController.Instance.isInternetAvailable ()) {
				Application.OpenURL (GameInfo.ReviewURL);			
			}
		}	
	}

	/// <summary>
	/// Raises the shop button pressed event.
	/// </summary>
	public void OnShopButtonPressed()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
			//StackManager.Instance.SpawnUIScreen ("Shop");	
			StackManager.Instance.powerScreen.Activate();	
		}
	}

	/// <summary>
	/// Raises the select language button pressed event.
	/// </summary>
	public void OnSelectLanguageButtonPressed()
	{
		AudioManager.Instance.PlayButtonClickSound ();
		StackManager.Instance.selectLanguageScreen.Activate();
	}


	public void OnThemeChange()
	{


	}
	/// <summary>
	/// Raises the share button pressed event.
	/// </summary>
	public void OnShareButtonPressed()
	{
		if (InputManager.Instance.canInput ()) {
			AudioManager.Instance.PlayButtonClickSound ();
			//NativeShare.Share("", "", "", "", "", true, "");
			//NativeShare.Share("", "", "https://play.google.com/store/apps/details?id=com.emedia.dollartune", "", "image/png", true, "");
			ShareScreenshotWithText("https://play.google.com/store/apps/details?id=com.adityapromotions.blockwall");
		}
	}

	 public void ShareScreenshotWithText(string text)
    {
        string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
        if(File.Exists(screenShotPath)) File.Delete(screenShotPath);

        ScreenCapture.CaptureScreenshot(ScreenshotName);

        StartCoroutine(delayedShare(screenShotPath, text));
    }

    //CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
    //for it to save, or set a unique image name and check if the file has been created yet before sharing.
    IEnumerator delayedShare(string screenShotPath, string text)
    {
        while(!File.Exists(screenShotPath)) {
    	    yield return new WaitForSeconds(.05f);
        }
		Debug.Log("Text "+text+" Screenshot Path "+screenShotPath);

		NativeShare.Share(text,screenShotPath, "", "", "image/png", true, "");
    }

}
