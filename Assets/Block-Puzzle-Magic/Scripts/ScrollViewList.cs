﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ScrollViewList : MonoBehaviour
{

    public ShapeBlockList currentLevelBlocks;
    private ShapeBlockList availableShapeList;

    int listLength;
    List<int> currentShapesOnBoardIds;

    public Transform availableShapeCard;
    public Transform currentShapeCard;
    public Transform contentOnBoard;
    public Transform contentAvailable;
    public static event Action ScrollViewDisableEvent;

    public Transform currentShapesListTitle;
    public Transform availableShapesListTitle;

    public GameObject nextStepInstruction;

    public static ScrollViewList Instance;

    public static event Action OnShapeReplace;
    public Button selectedCurrentButton;
    private int currentShapecardposition;

    public ShapeInfo selectedCurrentShapeInfo;

    public float shapeScale=0.4f;

    // Use this for initialization
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnListClose()
    {
        if (ScrollViewDisableEvent != null)
        {
            ScrollViewDisableEvent.Invoke();
        }
        nextStepInstruction.SetActive(false);
        StackManager.Instance.scrollviewList.SetActive(false);
    }

    private void OnEnable()
    {
        gameObject.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = 1;
        currentShapecardposition = currentShapesListTitle.GetSiblingIndex() + 1;
        GetCurrentLevelShapes();
        //Debug.Log(availableShapeList.ShapeBlocks.Count);
         //GenerateAvailableShapes();
        AvailableShapesSetup();
        //GenerateOnBoardShapes();
        CurrentListSetup();
        AvailableListSetup();
        //OnBoardShapeCard.OnCurrentShapeSelected += AvailableListSetup;
    }

    public void CurrentListSetup()
    {
        currentShapesListTitle.gameObject.SetActive(true);
        availableShapesListTitle.gameObject.SetActive(true);   
       GenerateOnBoardShapes();
    }

   public void AvailableListSetup()
    {
        gameObject.GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = 1;
        availableShapesListTitle.gameObject.SetActive(true);
        currentShapesListTitle.gameObject.SetActive(true);
        GenerateAvailableShapes();
    }

    private void OnDisable()
    {
        //OnBoardShapeCard.OnCurrentShapeSelected -= AvailableListSetup;
    }

    void GetCurrentLevelShapes()
    {
        currentLevelBlocks = BlockShapeSpawner.Instance.shapeBlockListNew[BlockShapeSpawner.Instance.levelNo - 1];
    }

    void AvailableShapesSetup()
    {
        currentShapesOnBoardIds = new List<int>();
        availableShapeList = currentLevelBlocks;
        listLength = availableShapeList.ShapeBlocks.Count - 1;
        for (int index = 0; index < 3; index++)
        {
            if (BlockShapeSpawner.Instance.ShapeContainers[index].childCount > 0)
            {
                currentShapesOnBoardIds.Add(BlockShapeSpawner.Instance.ShapeContainers[index].GetChild(0).gameObject.GetComponent<ShapeInfo>().ShapeID);
            }
        }

        for (int index = 0; index <= listLength && index >= 0; index++)
        {
            int id = availableShapeList.ShapeBlocks[index].shapeBlock.GetComponent<ShapeInfo>().ShapeID;
            Debug.Log("available " + id);

            foreach (int i in currentShapesOnBoardIds)
            {
                if (id == i)
                {
                    SwapAndRemove(index);
                    index--;
                    break;
                }
            }
        }
    }

    void GenerateAvailableShapes()
    {
        for (int index = 0; index <= listLength; index++)
        {
            Transform shapeContainer = Instantiate(availableShapeCard, contentAvailable);
            shapeContainer.gameObject.SetActive(true);
            GameObject temp = BlockShapeSpawner.Instance.CreateShapeWithID(shapeContainer, availableShapeList.ShapeBlocks[index].shapeBlock.GetComponent<ShapeInfo>().ShapeID);
           temp.transform.localScale =  Vector3.one*shapeScale;
            temp.GetComponent<Canvas>().overrideSorting = false;
            //shapeContainer.SetSiblingIndex(availableShapesListTitle.GetSiblingIndex() + 1);
        }
    }

    void GenerateOnBoardShapes()
    {
        foreach (int id in currentShapesOnBoardIds)
        {
            Transform shapeContainer = Instantiate(currentShapeCard, contentOnBoard);
            shapeContainer.gameObject.SetActive(true);
            GameObject temp = BlockShapeSpawner.Instance.CreateShapeWithID(shapeContainer, id);
            temp.transform.localScale = Vector3.one*shapeScale;
            temp.GetComponent<Canvas>().overrideSorting = false;
            shapeContainer.SetSiblingIndex(currentShapecardposition);
            currentShapecardposition++;
        }
    }

    void SwapAndRemove(int index)
    {
        ShapeBlockSpawn temp = availableShapeList.ShapeBlocks[index];
        availableShapeList.ShapeBlocks[index] = availableShapeList.ShapeBlocks[listLength];
        availableShapeList.ShapeBlocks[listLength] = temp;
        listLength--;
    }


    public void SetReplacementInformation()
    {

        OnShapeReplace.Invoke();
        OnListClose();
    }

    // public IEnumerator MoveShapeContainerOut()
    // {

    // }
}
