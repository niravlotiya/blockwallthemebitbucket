﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvailableShapeCard : MonoBehaviour
{

    public Button button;
    // Use this for initialization
    void Start()
    {
        button = gameObject.GetComponent<Button>();
    }


    // Update is called once per frame
    void Update()
    {

    }

    public void AvailableShapeClick()
    {
        if (ScrollViewList.Instance.selectedCurrentButton != null)
        {
            ShapeReplaceManager.replacementShapeId = gameObject.GetComponentInChildren<ShapeInfo>().ShapeID;
            ScrollViewList.Instance.SetReplacementInformation();
        }

    }
}
