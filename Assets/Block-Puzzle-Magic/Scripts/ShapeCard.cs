﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShapeCard : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnEnable() {
		ScrollViewList.ScrollViewDisableEvent += OnScrollViewDisable;
	}

	private void OnDisable() {
		ScrollViewList.ScrollViewDisableEvent -= OnScrollViewDisable;
	}

	void OnScrollViewDisable()
	{
		Destroy(gameObject);
	}
}